var data = {
  "sideMenu": {
    "error": {},
    "homePage": "Accueil",
    "documentation": "Documentation",
    "upload": "Tester tes fichiers"
  },
  "footer": {
    "error": {},
    "desc": "ArtofCode est un assistant intelligent pour les développeurs, révolutionnant le contrôle de la qualité du code. Il est destiné à l'ensemble de la communauté des développeurs.",
    "join": "REJOINS NOUS ",
    "contact": "CONTACT"
  },
  "navbar": {
    "error": {},
    "account": "Compte",
    "stats": "Statistiques",
    "upload": "Analyseur",
    "doc": "Documentation",
    "download": "Téléchargement",
    "logout": "Déconnexion",
    "signin": "Connexion"
  },


  "login": {
    "error": {},
    "email": "Adresse Email",
    "password": "Mot de passe",
    "remember": "Se souvenir de moi",
    "signin": "Connexion",
    "forgotPassword": "Mot de passe oublié ?",
    "signup": "Pas de compte ? Inscrit toi",
    "withoutlog": "Continuer sans connexion",
    "modal": {
      "title": "Vérifier votre Email",
      "subject": "Vérifier le code envoyé à votre Email",
      "field": "Code de comfirmation",
      "resend": "Renvoyer",
      "verify": "Vérifier"
    }
  },
  "register": {
    "error": {},
    "username": "Nom de compte",
    "realName": "Vrai Nom",
    "email": "Adresse Email",
    "password": "Mot de passe",
    "confirmPassword": "Confirmer le mot de passe",
    "signup": "S'inscrire",
    "signin": "Déjà un compte ? Connecte toi",
    "modal": {
      "title": "Vérifier votre Email",
      "subject": "Vérifier le code envoyé à votre Email",
      "field": "Code de comfirmation",
      "resend": "Renvoyer",
      "verify": "Vérifier"
    }
  },
  "recover": {
    "error": {},
    "email": "Adresse Email",
    "recover": "Récupérez votre mot de passe",
    "rememberAccount": "Je me souviens de mon compte",
    "modal": {
      "title": "Récupérez votre compte",
      "subject": "Vérifier le code envoyé à votre Email",
      "code": "Code de comfirmation",
      "newPassword": "Nouveau mot de passe",
      "confirmPassword": "Confirmer le mot de passe",
      "cancel": "Annuler",
      "recover": "Récupérer"
    }
  },


  "homePage": {
    "error": {},
    "tableTitle": "Saviez-vous qu'ArtofCode a une version professionnelle ?",
    "tableSecondTitle": "Vous pouvez obtenir un réel avantage sur vos concurrents. Jetez un œil à tous les avantages qu'ils vous manquent!",
    "table": {
      "benefits": "Fonctionnalités",
      "community": "Edition Communautaire",
      "professional": "Edition Professionnelle",
      "onlineDoc": "Documentation en ligne",
      "software": "Logiciel",
      "stats": "Statistiques",
      "advanceStats": "Statistiques Avancées",
      "monthlyReport": "Rapport statistique mensuel",
      "team": "Equipe",
      "companyTeam": "Équipe d'entreprise"
    },
    "freeButton": "Rester avec l'édition gratuite",
    "proButton": "Devenir professionnel",
    "valueTitle": "Les valeurs d'ArtofCode",
    "valueText": "Un développeur perd trop de temps à corriger ses bugs: soit il les a déjà rencontrés et sait les résoudre, soit il cherche le problème dans les forums et la documentation, recherche des solutions, les adapte à son contexte, et recommence. Ces erreurs ne seront connues qu'après avoir livré le produit à son public, créant ainsi une frustration. Le produit alterne alors entre la phase de développement et la phase de production, fait perdre du temps à l'entreprise, et présente des fonctionnalités faibles ou inexistantes.",
    "card1Title": "Performance",
    "card1Text": "Détectez automatiquement les problèmes au plus tôt. Livrez un programme de qualité.",
    "card2Title": "Résilience",
    "card2Text": "Utilisez une infrastructure cloud, hautement disponible, qui s’adapte dynamiquement à vos enjeux.",
    "card3Title": "Rapidité",
    "card3Text": "Réduisez drastiquement le temps entre le développement et le déploiement.",
},
  "upload": {
    "error": {},
    "gg": "Aucune erreur détectée, félicitations !",
    "ggTooltip": "FÉLICITATIONS",
    "goToDoc": "Consultez la documentation de ce problème",
    "beSmarter": "Jetez un œil aux erreurs sur vos fichiers",
    "process": "Cliquez ici pour tester votre fichier",
    "processTooltip": "Cliquez et choisissez un fichier ou faites glisser et déposez",
    "goBack": "Essayez un autre fichier",
    "goBackTooltip": "Revenir en arrière",
    "goToStat": "Voir vos statistiques",
    "errorType": "Type d'erreur",
    "message": "Message",
    "start": "Début",
    "end": "Fin",
    "correction": "Corrigé"
  },
  "account": {
    "error": {},
    "edit": "Éditer",
    "save": "Sauvegarder les modifications",

    "informationTitle": "Vos informations",
    "informationUpdatePhoto": "Mettez à jour votre photo",
    "informationUsername": "Nom de compte",
    "informationRealName": "Vrai nom",
    "informationEditPassword": "Modifiez votre mot de passe",
    "modalInformations": {
      "title": "Modifiez votre mot de passe",
      "newPassword": "Nouveau mot de passe",
      "confirmPassword": "Confirmer le mot de passe",
      "oldPassword": "Ancien mot de passe",
      "cancel": "Annuler",
      "confirm": "Modifiez votre mot de passe"
    },

    "optionTitle": "Options",
    "optionLanguage": "Langage",
    "optionUpgrade": "Passez à un compte professionnel",
    "optionDelete": "Supprimer votre compte",
    "optionLogout": "Déconnexion",
    "optionLanguageSelect": {
      "fr": "Français",
      "en": "Anglais"
    },
    "modalOptions": {
      "title": "Supprimer le compte",
      "subject": "Voulez-vous vraiment supprimer votre compte?",
      "cancel": "Annuler",
      "confirm": "Confirmer"
    },

    "emailTitle": "Email",
    "modalEmail": {
      "title": "Change ton adresse Email",
      "subject": "Veuillez entrer le code envoyé sur votre nouvelle adresse Email",
      "newEmail": "Nouvelle adresse Email",
      "code": "Code de sécurité",
      "confirm": "Modifier votre e-mail",
      "cancel": "Annuler",
      "back": "Revenir en arrière",
      "validate": "Validez votre email"
    },

    "phoneTitle": "Numéro de téléphone",
    "phoneNotMentioned": "Non mentionné",
    "modalPhone": {
      "title": "Changez votre numéro de téléphone",
      "field": "Nouveau numéro de téléphone",
      "cancel": "Annuler",
      "confirm": "Valider"
    }
  },
  "stat": {
    "Testnbr": "Tests par",
    "errornbr": "Erreurs par",
    "errorRepartition": "Répartition des erreurs",
    "legend": "Légende",
    "startingDay": "Jour de départ",
    "endingDay": "Jour de fin",
    "temporality": "Sélectionnez la temporalité",
    "temporalitySelect": {
      "day": "Jour",
      "week": "Semaine",
      "month": "Mois",
      "year": "Année"
    },
    "modal": {
      "title": "Changer la couleur",
      "close": "Fermer"
    }
  },


  "docList": {
    "error": {},
    "search": "Rechercher..."
  },
  "docPage": {
    "error": {},
    "back": "Revenir en arrière",
    "edit": "Modifier",
    "save": "Sauvegarder",
    "goBack": "Revenir en arrière",
    "goEdit": "Editer la documentation"
  },
  "createDocPage": {},
  "download": {
    "download": "Télécharger notre extension VsCode",
    "install": "Guide d'installation",
    "documentation": "Documentation"
  },


  "notFound": {
    "errornotfound": "404 - Page Non trouvé !"
  }

}
export default data