var data = {
    "sideMenu": {
         "error": {},
         "homePage": "Home Page",
         "documentation": "Documentation",
         "upload": "File checker"
    },
    "footer": {
         "error": {},
         "desc": "ArtofCode is a smart assistant for developers revolutionizing code quality control. It is intended for the entire developer community.",
         "join": "JOIN US ",
         "contact": "CONTACT"
    },
    "navbar": {
         "error": {},
         "account": "Account",
         "stats": "Statistics",
         "upload": "Analyser",
         "doc": "Documentation",
         "download": "Download",
         "logout": "Logout",
         "signin": "Sign In"
    },
 
 
     "login": {
         "error": {},
         "email": "Email Address",
         "password": "Password",
         "remember": "Remember me",
         "signin": "Sign in",
         "forgotPassword": "Forgot password ?",
         "signup": "Don't have an account ? Sign Up",
         "withoutlog": "Continue without login",
         "modal": {
             "title": "Verify your Email",
             "subject": "Verify the code sent on your Email",
             "field": "Comfirmation code",
             "resend": "Send again",
             "verify": "Verify"
         }
     },
     "register": {
         "error": {},
         "username": "Username",
         "realName": "Real Name",
         "email": "Email Address",
         "password": "Password",
         "confirmPassword": "Confirm Password",
         "signup": "Sign Up",
         "signin": "Already have an account ? Sign In",
         "modal": {
             "title": "Verify your Email",
             "subject": "Verify the code sent on your Email",
             "field": "Comfirm code",
             "resend": "Send again",
             "verify": "Verify"
         }
     },
     "recover": {
         "error": {},
         "email": "Email Address",
         "recover": "Recover Password",
         "rememberAccount": "Remember your account ?",
         "modal": {
             "title": "Recover your Account",
             "subject": "Verify the code sent on your Email",
             "code": "Comfirmation code",
             "newPassword": "New password",
             "confirmPassword": "Comfirm password",
             "cancel": "Cancel",
             "recover": "Recover"
         }
     },
 
 
     "homePage": {
         "error": {},
         "tableTitle": "Did you know that ArtofCode has a professional version ?",
         "tableSecondTitle": "You can get a real difference against your entrepreneurial competitor. Take a look at all the benefits you're missing !",
         "table": {
             "benefits": "Benefits",
             "community": "Community Edition",
             "professional": "Professional Edition",
             "onlineDoc": "Online Documentation",
             "software": "Software",
             "stats": "Statistics",
             "advanceStats": "Advanced Statistics",
             "monthlyReport": "Monthly Statistics report",
             "team": "Team",
             "companyTeam": "Company Team"
         },
         "freeButton": "Stay with Free Edition",
         "proButton": "Become professional",
         "valueTitle": "The values ​​of ArtofCode",
         "valueText": "A developer loses too much time correcting his bugs: Either he has already met them and knows how to solve them, or he looks for the problem among forums and documentation, search for solutions, adapt them for its context, and starts again. These errors will only be known after delivering the product to its audience, creating frustration. The product then alternates between the development phase and the production phase, wastes the company's time, and has weak or non-existent functionalities.",
         "card1Title": "Performance",
         "card1Text": "Automatically detect problems early. Deliver a quality program.",
         "card2Title": "Resilience",
         "card2Text": "Use a highly available cloud infrastructure that dynamically adapts to your challenges.",
         "card3Title": "Speed",
         "card3Text": "Drastically reduce the time between development and deployment.",
     },
     "upload": {
         "error": {},
         "gg": "No Error detected, Congratulation !",
         "ggTooltip": "CONGRATULATION",
         "goToDoc": "Check the documentation for this problem",
         "beSmarter": "Take a look at the errors on your files, be smarter than a normal user ;)",
         "process": "Click here to process your file",
         "processTooltip": "Click and choose a file or drag&drop",
         "goBack": "Try another file",
         "goBackTooltip": "Go back",
         "goToStat": "See your statistics",
         "errorType": "Error type",
         "message": "Message",
         "start": "Start",
         "end": "End",
         "correction": "Correction"
     },
     "account": {
         "error": {},
         "edit": "Edit",
         "save": "Save change",
 
         "informationTitle": "Your Informations",
         "informationUpdatePhoto": "Update your photo",
         "informationUsername": "Username",
         "informationRealName": "Real name",
         "informationEditPassword": "Edit your password",
         "modalInformations": {
             "title": "Edit your password",
             "newPassword": "New Password",
             "confirmPassword": "Confirm Password",
             "oldPassword": "Old Password",
             "cancel": "Cancel",
             "confirm": "Edit your password"
         },
 
         "optionTitle": "Options",
         "optionLanguage": "Language",
         "optionUpgrade": "Upgrade to a professional account",
         "optionDelete": "Delete your account",
         "optionLogout": "Logout",
         "optionLanguageSelect": {
             "fr": "French",
             "en": "English"
         },
         "modalOptions": {
             "title": "Delete Account",
             "subject": "Are you sure you want to delete your account ?",
             "cancel": "Cancel",
             "confirm": "Confirm"
         },
 
         "emailTitle": "Email",
         "modalEmail": {
             "title": "Change your email address",
             "subject": "Please refer the code sent on your new email address",
             "newEmail": "New email address",
             "code": "Security Code",
             "confirm": "Edit your Email",
             "cancel": "Cancel",
             "back": "Back",
             "validate": "Validate your Email"
         },
 
         "phoneTitle": "Phone Number",
         "phoneNotMentioned": "Not Yet Mentioned",
         "modalPhone": {
             "title": "Change your phone number",
             "field": "New phone number",
             "cancel": "Cancel",
             "confirm": "Confirm"
         }
     },
     "stat": {
         "Testnbr": "Tests per",
         "errornbr": "Errors per",
         "errorRepartition": "Errors repartition",
         "legend": "Legend",
         "startingDay": "Starting Day",
         "endingDay": "Ending Day",
         "temporality": "Select the temporality",
         "temporalitySelect": {
             "day": "day",
             "week": "week",
             "month": "month",
             "year": "year"
         },
         "modal": {
             "title": "Change the color",
             "close": "Close"
         }
     },

 
     "docList": {
         "error": {},
         "search": "Search..."
     },
     "docPage": {
         "error": {},
         "back": "Back",
         "edit": "Edit",
         "save": "Save",
         "goBack": "Go back",
         "goEdit": "Edit the file"
     },
     "createDocPage": {},
     "download": {
        "download": "Download our VsCode extension",
        "install": "How to install",
        "documentation": "Documentation"
     },

     
     "notFound": {
         "errornotfound": "404 - Page Not Found !"
     }
 }
 export default data