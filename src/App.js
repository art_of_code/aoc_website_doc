import React, { Suspense, useEffect } from 'react';
import { BrowserRouter as Router, withRouter } from "react-router-dom";
import TagManager from 'react-gtm-module';
//import ReactGA from 'react-ga';
import { connect } from "react-redux"
import { _onCheckLog } from "./Redux/Store/actionCreators"

import Routes from "./Routes";

import Amplify from 'aws-amplify';
import AOCFooter from './Components/Footer/footer';
import AOCAppBar from './Components/AppBar/appBar';

const poolID = process.env.REACT_APP_POOL_ID;
const poolAppID = process.env.REACT_APP_POOL_APP_ID;
const iPoolId = process.env.REACT_APP_I_POOL_ID;
const Bucket = process.env.REACT_APP_BUCKET;
const Region = 'eu-west-2';

Amplify.configure({
  Auth: {
    identityPoolId: iPoolId,
    region: Region,
    userPoolId: poolID,
    userPoolWebClientId: poolAppID,
  },
  Storage: {
    AWSS3: {
      bucket: Bucket,
      region: Region,
    }
  }
});

let AppBarWithRouter = withRouter(AOCAppBar);

function Page() {
  return (
    <Router forceRefresh={true}>
      <AppBarWithRouter />
      <Routes />
      <AOCFooter />
    </Router>
  );
}

// loading component for suspence fallback
const Loader = () => (
  <div>
    <div>loading...</div>
  </div>
);

// here app catches the suspense from page in case translations are not yet loaded
export const API_URL = process.env.REACT_APP_API_URL || "https://api.artofcode.eu/dev/";

const mapStateToProps = state => {
  return {
    account: state.userInfo,
    image: state.imgLink
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveAccount: () => { dispatch(_onCheckLog()) },
  }
}

function App(props) {
  const tagManagerArgs = {
    gtmId: 'GTM-NQQM3VT'
  }
  // TagManager.initialize(tagManagerArgs);
  // ReactGA.initialize('G-DXGLGVYXP0');

  useEffect(() => {
    props.saveAccount();
  }, []);
  return (
    <Suspense fallback={<Loader />}>
      <Page />
    </Suspense>
  );
} export default connect(mapStateToProps, mapDispatchToProps)(App)

