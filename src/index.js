import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import {IntlProvider} from "react-intl";

import { createStore, applyMiddleware } from "redux"
import { Provider } from "react-redux"
import thunk from "redux-thunk"

import App from './App';
import reducer from "./Redux/Store/reducer"

const theme = createMuiTheme({
  palette: {
    // bleu foncé
    primary: {
      main: '#1F4F87'
    },
    // gris claire
    secondary: {
      main: '#F4F4F4'
    },
    //bleu claire
    third: {
      main: '#459CF9',
    },
    //blanc claire
    fourth: {
      main: '#f7f3e9',
    },
    // font blanc
    font1: {
      color: '#FCFCFC',
    },
    // font noir
    font2: {
      color: '#070707',
    },
  },
  typography: { fontFamily: ['Montserrat'].join(','), },
  typo: { fontFamily: ['Poppins, sans-serif'].join(','), },
});

const store = createStore(reducer, applyMiddleware(thunk))

ReactDOM.render(
  <IntlProvider locale="en">
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/railscasts.css" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap" rel="stylesheet"></link>
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&display=swap" rel="stylesheet"></link>
        <App />
      </MuiThemeProvider>
    </Provider>
  </IntlProvider>
  , document.getElementById('root'));