import React from 'react';
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { Avatar, Box, AppBar, Toolbar, Button, Grid } from '@material-ui/core';
import Popover from '@material-ui/core/Popover';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';
import { Auth, Storage } from 'aws-amplify';

import { styles } from './appBarStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

const excludePaths = ["/Login", "/Register", "/Recover"];

class AOCAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      connected: false,
      avatar: null,
      imageLink: "",
    }
  }

  componentDidMount() {
    this._onGetInformation();
  }

  _onLogout = async () => {
    Auth.signOut()
      .then(() => {
        this.props.history.push('/Login');
      })
      .catch(err => console.log(err));
  }

  _onGetInformation = async () => {
    Auth.currentAuthenticatedUser()
      .then((user) => {
        this.setState({ connected: true, username: user.attributes.nickname });
        Storage.get(user.username)
          .then(result => this.setState({ imageLink: result }))
          .catch(() => console.log("Error while loading image"));
      })
      .catch(err => console.log(err));
  };

  choose_link(classes) {
    if (this.state.connected) {
      return (
        <Grid container direction="row" justify="center" alignContent="center">
          <Grid item xs={1} lg={1} style={{ alignSelf: 'center', maxWidth: '60px' }}>
            <Link to="/"><img className={classes.logo} src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/logo.svg" alt="logo" /></Link>
          </Grid>
          <Grid item xs={11} style={{ alignSelf: 'center' }} >
            <PopupState variant="popover">
              {popupState => (
                <div>
                  <Grid container direction="row" justify="flex-end">
                    <Grid item>
                      <Button className={classes.text} href="/Analyser">{en.navbar.upload}</Button><br />
                    </Grid>
                    <Grid item style={{ marginLeft: "1%" }}>
                      <Button className={classes.text} href="/DocumentationList">{en.navbar.doc}</Button><br />
                    </Grid>
                    <Grid item style={{ marginLeft: "1%" }}>
                      <Avatar src={this.state.imageLink} className={classes.avatar} {...bindTrigger(popupState)} />
                    </Grid>
                  </Grid>
                  <Popover
                    {...bindPopover(popupState)}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'center',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                  >
                    <Box className={classes.popOver}>
                      <Button fullWidth className={classes.textInside} href="/Account"> {en.navbar.account} </Button><br />
                      <Button fullWidth className={classes.textInside} href="/Stats"> {en.navbar.stats} </Button><br />
                      <Button fullWidth className={classes.textInside} href="/Download"> {en.navbar.download} </Button>
                      <Button fullWidth className={classes.textInside} onClick={this._onLogout}> {en.navbar.logout} </Button><br />
                    </Box>
                  </Popover>
                </div>
              )}
            </PopupState>
          </Grid>
        </Grid >
      )
    }
    else
      return (
        <Grid container direction="row" justify="center" alignContent="center">
          <Grid item xs={1} lg={1} style={{ alignSelf: 'center', maxWidth: '60px' }}>
            <Link to="/"><img className={classes.logo} src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/logo.svg" alt="logo" /></Link>
          </Grid>
          <Grid item xs={11} style={{ alignSelf: 'center' }}>
            <Grid container direction="row" justify="flex-end" alignContent="center">
              <Grid item xs={5} lg={2} style={{ textAlign: 'end' }}>
                <Button className={classes.text} href="/DocumentationList"> {en.navbar.doc} </Button><br />
              </Grid>
              <Grid item xs={5} lg={1} style={{ textAlign: 'end' }}>
                <Button href="/Login" className={classes.button_link + ' ' + classes.text}>{en.navbar.signin}</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )
  }

  render() {
    const { classes } = this.props;

    if (excludePaths.includes(this.props.location.pathname))
      return null;

    return (
      <div>
        <AppBar className={classes.back}>
          <Toolbar>
            {this.choose_link(classes)}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
export default withStyles(styles)(AOCAppBar);