export const styles = theme => ({
    back: {
      background: theme.palette.primary.main,
    },
    text: {
      color: theme.palette.font1.color,
      fontFamily: theme.typo.fontFamily,
    },
    textInside: {
      margin: "auto",
      marginTop: "1.5px",
      marginBottom: "1.5px",
      color: theme.palette.font2.color,
      fontFamily: theme.typo.fontFamily,
    },
    button_link: {
      marginLeft: 'auto',
      marginRight: '3%',
    },
    avatar: {
      background: theme.palette.primary.main,
      color: theme.palette.font1.color,
      cursor: "pointer",
      marginLeft: "auto",
    },
    logo: {
      maxWidth: "80%",
      objectFit: "contain",
      filter: "brightness(2)",
      [theme.breakpoints.down('md')]: {
        maxWidth: "100%",
      }
    }
  });