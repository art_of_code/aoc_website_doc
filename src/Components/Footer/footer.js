import React from 'react';
import { withStyles } from "@material-ui/core/styles";
import MailIcon from '@material-ui/icons/Mail';
import { Container, Grid, Typography, Link } from '@material-ui/core/';

import { styles } from './footerStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

function Copyright() {
  return (
    <Typography variant="body2" align="center">
      Copyright © <Link color="inherit" href="https://artofcode.eu/">ArtofCode</Link>{' '}{new Date().getFullYear()}{'.'}
    </Typography>
  );
}

class AOCFooter extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <footer className={classes.footer} data-test="footer">
        <Container>
          <Grid container spacing={3} data-test="grid" alignContent="center" justify="center" style={{ paddingBottom: '30px' }}>
            <Grid item md={4} lg={4} mb={4}>
              <Typography className={classes.smallDesc} variant="h6" gutterBottom>ARTOFCODE</Typography>
              <br />
              <Typography className={classes.smallDesc} variant="body1">{en.footer.desc}</Typography>
            </Grid>
            <Grid item md={4} lg={2} mb={4}>
              <Typography variant="h6" className={classes.smallDesc} gutterBottom>{en.footer.join}</Typography>
              <br />
              <div className={classes.linkIcon} data-test="icon">
                <Link target="_blank" underline="none" className={classes.linkIcon} href="https://www.facebook.com/Art-Of-Code-2373428686047793/">
                  <img alt="facebook_logo" src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/public_img_facebook.png" className={classes.importImg} /> Facebook
                </Link>
              </div>
              <div className={classes.linkIcon} data-test="icon">
                <Link target="_blank" underline="none" className={classes.linkIcon} href="https://twitter.com/ArtOfCode2">
                  <img alt="twitter_logo" src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/public_img_twitter.png" className={classes.importImg} /> Twitter
                </Link>
              </div>
              <div className={classes.linkIcon} data-test="icon">
                <Link target="_blank" underline="none" className={classes.linkIcon} href="https://discord.gg/teNFRfs">
                  <img alt="discord_logo" src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/public_img_discord.png" className={classes.importImg} /> Discord
                </Link>
              </div>
              <div className={classes.linkIcon} data-test="icon">
                <Link target="_blank" underline="none" className={classes.linkIcon} href="https://gitlab.com/art_of_code">
                  <img alt="gitlab_logo" src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/public_img_gitlab.png" className={classes.importImg} /> Gitlab
                </Link>
              </div>
            </Grid>
            <Grid item md={4} lg={3} mb={4}>
              <Typography variant="h6" className={classes.smallDesc} gutterBottom>{en.footer.contact}</Typography>
              <br />
              <div className={classes.linkIcon} data-test="icon">
                <MailIcon className={classes.linkPic} />
                <Typography className={classes.linkEmail} style={{ marginLeft: "0.5em" }} variant="body2" gutterBottom>artofcode_2021@labeip.epitech.eu</Typography>
              </div>
            </Grid>
          </Grid>
          <Copyright data-test="copyright" />
        </Container>
      </footer>
    );
  }
}
export default withStyles(styles)(AOCFooter);