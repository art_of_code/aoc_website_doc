import { createShallow } from '@material-ui/core/test-utils';
import React from 'react';
// import { shallow } from 'enzyme';
import _Footer from './footer.js';
import { exportAllDeclaration } from '@babel/types';
import { findByTestAtrr } from '../../../Utils';

describe('footer component', () => {
  let component;
  let shallow;

  beforeEach(() => {
    shallow = createShallow();
    component = shallow(<_Footer />)
  });

  it('should render icons', () => {
    const wrapper = findByTestAtrr(component, 'icon');
    expect(wrapper.length).toBe(5);
  });

  it('should render a grid', () => {
    const wrapper = findByTestAtrr(component, 'grid');
    expect(wrapper.length).toBe(1);
  });

  it('should render the footer', () => {
    const wrapper = findByTestAtrr(component, 'footer');
    expect(wrapper.length).toBe(1);
  });

  it('should render the copyright', () => {
    const wrapper = findByTestAtrr(component, 'copyright');
    expect(wrapper.length).toBe(1);
  });
});