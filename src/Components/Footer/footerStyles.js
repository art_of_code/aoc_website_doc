export const styles = theme => ({
    footer: {
      padding: theme.spacing(2),
      color: theme.palette.font1.color,
      background: theme.palette.primary.main,
    },
    smallDesc: {
      fontFamily: theme.typo.fontFamily,
    },
    linkIcon: {
      fontFamily: theme.typo.fontFamily,
      color: theme.palette.font1.color,
      display: 'flex',
      alignItems: 'center',
      marginTop: '0.3vh',
      fontSize: "100%",
    },
    linkEmail: {
      fontFamily: theme.typo.fontFamily,
      color: theme.palette.font1.color,
      display: 'flex',
      alignItems: 'center',
      marginTop: '0.3vh',
      fontSize: "100%",
      [theme.breakpoints.down('sm')]: {
        fontSize: "80%",
      }
    },
    linkPic: {
      display: 'flex',
      alignItems: 'center',
      paddingRight: '0.5m',
    },
    importImg: {
      width: "2em",
      height: "2em",
      marginRight: "10%",
      [theme.breakpoints.down('md')]: {
        marginRight: "6%",
      },
    }
});