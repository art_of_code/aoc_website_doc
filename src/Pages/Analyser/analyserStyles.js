export const styles = theme => ({
    Page: {
      marginTop: "3.5em",
      background: "#f7f3e9",
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      }
    },
    uploadPage: {
      width: "100%",
      minHeight: "45em",
      paddingTop: "4%",
    },
    descBox: {
      width: "80%",
      margin: "auto",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      marginBottom: "7em",
      [theme.breakpoints.down('md')]: {
        width: "100%",
        marginTop: "7%",
        marginBottom: "10%",
      }
    },
    descText: {
      fontFamily: theme.typo.fontFamily,
      margin: "auto",
      color: "black",
    },
    DragDrop: {
      width: "40%",
      height: "25em",
      marginLeft: "30%",
      marginRight: "30%",
      display: "flex",
      justifyContent: "center",
      textAlign: "center",
      border: "2px dashed"+theme.palette.primary.main,
      backgroundColor: "rgba(0, 0, 0, 0.05)",
      borderRadius: "22px",
      outline: "0px solid transparent",
      cursor: "pointer",
      [theme.breakpoints.down('md')]: {
        width: "80%",
        marginLeft: "10%",
        marginRight: "10%",
      }
    },
    sendBox: {
      width: "40%",
      margin: "auto",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      [theme.breakpoints.down('md')]: {
        width: "100%",
      }
    },
    dropImg: {
      margin: "auto",
      height: "10rem",
      width: "10em",
    },
    dropText: {
      fontFamily: theme.typo.fontFamily,
      margin: "auto",
      marginTop: "1em",
      [theme.breakpoints.down('md')]: {
        textAlign: "center",
      }
    },
    ggText: {
      fontFamily: theme.typo.fontFamily,
      margin: "auto",
      marginTop: "10em",
      [theme.breakpoints.down('md')]: {
        textAlign: "center",
        marginTop: "5em",
      }
    },
    responsePage: {
      width: "100%",
      minHeight: "45em",
      paddingTop: "4%",
      paddingBottom: "3em",
    },
    responseSaveBox: {
      width: "25%",
      display: "flex",
      justifyContent: "flex-end",
      margin: "auto",
    },
    responseBackBox: {
      width: "75%",
      display: "flex",
      justifyContent: "flex",
      margin: "auto",
    },
    responseButton: {
      fontFamily: theme.typo.fontFamily,
      width: "75%",
      margin: "auto",
      marginBottom: "1em",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      [theme.breakpoints.down('xs')]: {
        width: "98%",
      },
    },
    errorMsg: {
      fontFamily: theme.typo.fontFamily,
      margin: "auto",
      marginTop: "2em",
    },
    errorMsg2: {
      fontFamily: theme.typo.fontFamily,
      fontSize: "1em",
      margin: "auto",
      textAlign: "center",
      marginTop: "2em",
    },
    loader: {
      margin: 'auto',
      marginTop: '1em',
      color: theme.palette.primary.main,
    },
    expandIcon: {
      color: theme.palette.secondary.main,
    },
    accordion: {
      marginTop: "0.5%",
      width: "75%",
      marginLeft: "auto !important",
      marginRight: "auto !important",
      [theme.breakpoints.down('md')]: {
        marginTop: "2%",
        width: "98%",
      }
    },
    accordionSummary: {
      color: theme.palette.secondary.main,
      background: theme.palette.primary.main,
    },
    accordionSummaryBox: {
      display: "flex",
      flexDirection: "row",
    },
    accordionSummaryTitle: {
      fontFamily: theme.typo.fontFamily,
      fontSize: "1.5em",
      color: theme.palette.secondary.main,
      margin: 'auto',
      [theme.breakpoints.down('md')]: {
        fontSize: "1em",
      }
    },
    accordionSummaryIcon: {
      color: "black",
      margin: 'auto',
      marginLeft: "0.5em",
      [theme.breakpoints.down('md')]: {
        marginLeft: "0.1em",
      }
    },
    accordionSummaryIconDoc: {
      fontSize: "1.3em",
      [theme.breakpoints.down('md')]: {
        fontSize: "1em",
      }
    },
    accordionTableCell: {
      fontSize: "1em",
      [theme.breakpoints.down('md')]: {
        fontSize: "0.7em",
        margin: "auto",
        textAlign: "center",  
      }
    },
});