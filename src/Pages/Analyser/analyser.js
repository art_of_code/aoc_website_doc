import React from "react";
import { CircularProgress, Tooltip, Button, withStyles, Box, Typography, Accordion, AccordionSummary, AccordionDetails, Checkbox } from '@material-ui/core';
import { Table, TableBody, TableRow, TableHead, TableCell, IconButton } from '@material-ui/core';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DescriptionIcon from '@material-ui/icons/Description';
import { Auth } from 'aws-amplify';
import { API_URL } from '../../App.js';
import Dropzone from 'react-dropzone';

import { styles } from './analyserStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

class Analyser extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      result: null,
      step: 1,
      loader: false,
      menuAppear: false,
      openCard: [],
    };
    this._onFile = this._onFile.bind(this);
    this._toggleMenu = this._toggleMenu.bind(this);
  }

  handleChange(files) {
    this.setState({
      files: files
    });
  }

  _toggleMenu = () => {
    this.setState({ menuAppear: !this.state.menuAppear });
  }

  _closeMenu = () => {
    if (this.state.menuAppear === true)
      this.setState({ menuAppear: false });
  }

  _onFile = async (file) => {
    this.setState({ error: null, result: null, loader: true })
    if (file[0].name.includes(".js", (file[0].name.length - 3)) === false) {
      this.setState({ error: "Error, not the good file format", loader: false });
      return;
    }
    let session = await Auth.currentSession();
    let data = new FormData();
    data.append("file", file[0]);
    await fetch(API_URL + 'backoffice/process', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${session.idToken.jwtToken}`,
      },
      body: data
    })
      .then(res => res.json())
      .then((res) => {
        if (res.error) {
          this.setState({ error: res.error });
        } else {
          if (JSON.parse(res.errors.statusCode) === 200) {
            let bodyRespond = [];
            let body = res.errors.body;
            for (let code in body) {
              bodyRespond = bodyRespond.concat(body[code])
            }
            this.setState({ result: bodyRespond, step: 2 });
          } else {
            this.setState({ error: "invalid status code" });
          }
        }
      })
      .catch((e) => {
        // FAILURE
        this.setState({ error: "Internal Error" });
      });
    this.setState({ loader: false });
  };

  _onGetDoc = async (keyword) => {
    let session = await Auth.currentSession();
    await fetch(API_URL + 'documentation/find/' + keyword, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${session.idToken.jwtToken}`,
      }
    })
      .then(res => res.json())
      .then((res) => {
        if (res.length === 0) {
          this.setState({ error: "This documentation is unavailable" });
          setTimeout(function () {
            this.setState({ error: null });
          }.bind(this), 2000)
        } else {
          window.location = "/DocumentationPage/" + res[0].id;
        }
      })
      .catch((e) => { console.log("error: " + e) })
  };

  _onFillCards = () => {
    const { classes } = this.props;
    if (this.state.result.length === 0) {
      return (
        <Box className={classes.sendBox}>
          <Tooltip title={en.upload.ggTooltip} aria-label={en.upload.ggTooltip}>
            <Typography className={classes.ggText} variant="h5">{en.upload.gg}</Typography>
          </Tooltip>
        </Box>
      )
    }
    let tmpcode = this.state.result[0].code;
    let tmpArray = [];
    let array = [];
    for (let i = 0; i < Object.keys(this.state.result).length; i++) {
      if (tmpcode !== this.state.result[i].code) {
        array.push(tmpArray.slice());
        tmpArray = [];
      }
      tmpArray.push(this.state.result[i]);
      tmpcode = this.state.result[i].code;
    }
    if (tmpArray[0].code !== array[array.length - 1]) {
      array.push(tmpArray);
    }
    return array.map((res, index) => {
      if (res[0].code !== "internal" && res[0].code !== "predictable") {
        return (
          <Accordion className={classes.accordion} key={index}>
            <AccordionSummary
              className={classes.accordionSummary}
              expandIcon={<ExpandMoreIcon className={classes.expandIcon} />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Box className={classes.accordionSummaryBox}>
                <Typography className={classes.accordionSummaryTitle}>{res[0].code} ({Object.keys(res).length})</Typography>
              </Box>
            </AccordionSummary>
            <AccordionDetails>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.accordionTableCell}>{en.upload.errorType}</TableCell>
                    <TableCell className={classes.accordionTableCell}>{en.upload.message}</TableCell>
                    <TableCell className={classes.accordionTableCell}>{en.upload.start}</TableCell>
                    <TableCell className={classes.accordionTableCell}>{en.upload.end}</TableCell>
                    <TableCell className={classes.accordionTableCell}>{en.upload.correction}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {res.map((r, index) => {
                    return (
                      <TableRow key={index}>
                        <TableCell className={classes.accordionTableCell}><b>
                          {r.type}
                          <IconButton onClick={() => { this._onGetDoc(r.type) }} size="small" className={classes.accordionSummaryIcon}>
                            <Tooltip title={en.upload.goToDoc} aria-label={en.upload.goToDoc}>
                              <DescriptionIcon className={classes.accordionSummaryIconDoc} />
                            </Tooltip>
                          </IconButton>
                        </b></TableCell>
                        <TableCell className={classes.accordionTableCell}>{r.msg}</TableCell>
                        <TableCell className={classes.accordionTableCell}>{r.loc.start.line}:{r.loc.start.column}</TableCell>
                        <TableCell className={classes.accordionTableCell}>{r.loc.end.line}:{r.loc.end.column}</TableCell>
                        <TableCell className={classes.accordionTableCell}><Checkbox color="primary" /></TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </AccordionDetails>
          </Accordion>
        )
      } else {
        return (
          <span key={index} />
        )
      }
    })
  }

  render() {
    const { classes } = this.props;

    if (this.state.step === 1) {
      return (
        <div className={classes.Page}>
          <Box className={classes.uploadPage}>
            <Box className={classes.descBox}>
              <Typography className={classes.descText} variant="h4" align="center">{en.upload.beSmarter}</Typography>
            </Box>
            <Dropzone
              onDrop={FileObject => (this._onFile(FileObject))}
              onError={() => (this.setState({ error: "We encountered an error" }))}
              minSize={0}
              maxSize={100000000}
            >
              {({ getRootProps, getInputProps }) => (
                <Tooltip title={en.upload.processTooltip} aria-label={en.upload.processTooltip}>
                  <div className={classes.DragDrop} {...getRootProps()}>
                    <input {...getInputProps()} />
                    <Box id="gtm-analyze" className={classes.sendBox}>
                      <img alt="drop file here" src="img/search.png" className={classes.dropImg}></img>
                      <Typography className={classes.dropText} variant="h5" color="textSecondary">{en.upload.process}</Typography>
                      <Typography variant="h6" className={classes.errorMsg}>{this.state.error}</Typography>
                      {(() => {
                        switch (this.state.loader) {
                          case true: return <CircularProgress className={classes.loader} />;
                          default: return null;
                        }
                      })()}
                    </Box>
                  </div>
                </Tooltip>
              )}
            </Dropzone>
          </Box>
          {/* End code page here */}
        </div>
      );
    }
    if (this.state.step === 2) {
      return (
        <div className={classes.Page}>
          <Box className={classes.responsePage}>
            <Box className={classes.responseButton}>
              <Box className={classes.responseBackBox}>
                <Tooltip title={en.upload.goBackTooltip} aria-label={en.upload.goBackTooltip}>
                  <Button onClick={() => { this.setState({ step: 1, result: null, error: null }); }} ><KeyboardBackspaceIcon />{en.upload.goBack}</Button>
                </Tooltip>
              </Box>
              <Box className={classes.responseSaveBox}>
                <Tooltip title={en.upload.goToStat} aria-label={en.upload.goToStat}>
                  <Button href="/Stats"><EqualizerIcon fontSize="large" /></Button>
                </Tooltip>
              </Box>
            </Box>
            <Typography className={classes.errorMsg2}>{this.state.error}</Typography>
            {this._onFillCards()}
          </Box>
          {/* End code page here */}
        </div>
      );
    }
  }
}
export default withStyles(styles)(Analyser);