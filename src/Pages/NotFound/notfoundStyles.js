export const styles = theme => ({
    Page: {
      marginTop: "3.5em",
      background: theme.palette.fourth.main,
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      }
    },
    contentPage: {
      minHeight: "45em",
      width: "100%",
    },
    notFound: {
      fontFamily: theme.typo.fontFamily,
    }
});