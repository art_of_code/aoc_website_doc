import React from "react";
import { withStyles } from '@material-ui/core';

import  { styles } from './notfoundStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

class NotFound extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      menuAppear: false,
    };
    this._toggleMenu = this._toggleMenu.bind(this);
  }

  _toggleMenu = () => {
    this.setState({ menuAppear: !this.state.menuAppear });
  }

  _closeMenu = () => {
    if (this.state.menuAppear === true)
      this.setState({ menuAppear: false });
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.Page}>
        <div className={classes.contentPage}>
          <div align="center">
            <img src="img/logo.png" alt="aligment" />
            <h1 className={classes.notFound}>{en.notFound.errornotfound}</h1>
            <h1 className={classes.notFound}>:/</h1>
          </div>
          {/* End code page here */}
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(NotFound);