import React from 'react';
import { Dialog, Box, Divider, withStyles, CssBaseline, CircularProgress, FormControl, Button, TextField, FormControlLabel, Checkbox, Link, Grid, Typography, Container } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { Auth } from 'aws-amplify';

import { styles } from './loginStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../../Language/fr.js").default : require("../../../Language/en.js").default;

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      error: "",
      remember: false,
      validate: false,
      loader: false,
      openModal: false,
      code: "",
    }
  }

  componentDidMount() {
    Auth.currentSession()
      .then(() => {
        this.setState({ validate: true });
      })
      .catch((e) => { console.log(e) })
  }

  _onCheckLogin = async () => {
    this.setState({ error: "", loader: true });
    if (this.state.email !== "" && this.state.password !== "") {
      Auth.signIn(this.state.email, this.state.password)
        .then((user) => {
          if (!user.attributes.email_verified) {
            this.setState({ loader: false, error: "email not confirmed", openModal: true });
          } else {
            this.setState({ loader: false, validate: true });
          }
        })
        .catch((err) => {
          if (err.code === "UserNotConfirmedException") {
            this.setState({ error: err.message, openModal: true, loader: false });
          } else {
            this.setState({ error: err.message, loader: false });
          }
        })
    } else {
      this.setState({ error: "Incomplete information" });
    }
    this.setState({ loader: false });
  }

  _onSendAgainEmail = async () => {
    Auth.resendSignUp(this.state.email)
      .then(() => {
        this.setState({ error: "code resent successfully" });
      })
      .catch((err) => {
        this.setState({ error: err.message });
      });
  }

  _onCheckEmail = async () => {
    Auth.confirmSignUp(this.state.email, this.state.code)
      .then(() => {
        this.setState({ openModal: false, code: "" });
        this._onCheckLogin();
      })
      .catch(() => {
        this.setState({ error: "Invalid Code" });
      });
  }

  render() {
    const { classes } = this.props;
    if (this.state.validate === true) {
      return <Redirect to='/' />
    }
    return (
      <Container component="main" maxWidth="xs" style={{ paddingBottom: '17%' }}>
        <CssBaseline />
        <div className={classes.Page}>
          <Typography component="h1" variant="h5">
            <img src="img/logo.png" alt="logo" />
          </Typography>
          <FormControl className={classes.FormControl} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label={en.login.email}
              type="email"
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label={en.login.password}
              type="password"
              autoComplete="current-password"
              value={this.state.password}
              onChange={e => this.setState({ password: e.target.value })}
            />
            <FormControlLabel
              control={<Checkbox
                value={this.state.remember}
                onChange={e => this.setState({ remember: e.target.value })}
                color="primary"
              />
              }
              label={en.login.remember}
            />
            {(() => {
              switch (this.state.loader) {
                case true: return <CircularProgress className={classes.loader} />;
                default: return null;
              }
            })()}
            <span className={classes.Error}>{this.state.error}</span>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.validateButton}
              onClick={() => this._onCheckLogin()}
            >
              {en.login.signin}
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="/Recover" variant="body2">
                  {en.login.forgotPassword}
                </Link>
              </Grid>
              <Grid item>
                <Link href="/Register" variant="body2">
                  {en.login.signup}
                </Link>
              </Grid>
            </Grid>
            <Link className={classes.continuenolog} href="/" variant="body2">
              {en.login.withoutlog}
            </Link>
          </FormControl>

          <Dialog
            open={this.state.openModal}
            onClose={() => { this.setState({ openModal: false }) }}
            keepMounted
            aria-labelledby={en.login.modal.title}
            aria-describedby={en.login.modal.title}
            fullWidth={true}
            maxWidth="md"
          >
            <Box className={classes.Modal}>
              <Typography className={classes.ModalTitle} variant="h5">{en.login.modal.title}</Typography>
              <Divider variant="middle" />
              <Box className={classes.ModalBox}>
                <Typography className={classes.ModalSubject}>{en.login.modal.subject}</Typography>
                <span className={classes.modalError}>{this.state.error}</span>
                <TextField className={classes.ModalField} type="text" label={en.login.modal.field} value={this.state.code} variant="outlined" onChange={e => this.setState({ code: e.target.value })} />
                <Box className={classes.ModalButtonBox}>
                  <Button className={classes.ModalButton} onClick={() => { this._onSendAgainEmail(); }} variant="contained">{en.login.modal.resend}</Button>
                  <Button className={classes.ModalButton} onClick={() => { this._onCheckEmail(); }} variant="contained">{en.login.modal.verify}</Button>
                </Box>
              </Box>
            </Box>
          </Dialog>
        </div>
      </Container>
    );
  }
}
export default withStyles(styles)(Login);