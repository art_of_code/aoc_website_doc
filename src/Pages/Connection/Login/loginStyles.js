export const styles = theme => ({
    Page: {
      marginTop: '64px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    FormControl: {
      width: '100%',
    },
    Error: {
      fontFamily: theme.typo.fontFamily,
      textAlign: "center",
    },
    validateButton: {
      fontFamily: theme.typo.fontFamily,
      background: theme.palette.primary.main,
      color: theme.palette.font1.color,
      margin: '24px 0px 16px',
    },
    continuenolog: {
      fontFamily: theme.typo.fontFamily,
      marginTop: '10%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      [theme.breakpoints.down('md')]: {
        marginBottom: "5%",
      }
    },
    loader: {
      margin: 'auto',
      color: theme.palette.primary.main,
    },
    Modal: {
      width: "100%",
      minHeight: "100%",
      padding: "auto",
      margin: "auto",
      background: theme.palette.primary.main,
    },
    ModalTitle: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      paddingTop: "1em",
      paddingBottom: "1em",
      textAlign: "center",
      color: theme.palette.primary.main,
      background: theme.palette.secondary.main,
    },
    ModalSubject: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      paddingTop: "1em",
      paddingBottom: "1em",
      textAlign: "center",
      color: theme.palette.secondary.main,
      fontSize: "2em",
      [theme.breakpoints.down('sm')]: {
        fontSize: "1em",
      }
    },
    ModalBox: {
      width: "90%",
      margin: "auto",
      marginTop: "5%",
      color: theme.palette.secondary.main,
      display: "flex",
      flexDirection: "column",
    },
    ModalField: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      marginTop: "1em",
      marginBottom: "1em",
      background: theme.palette.secondary.main,
    },
    ModalButtonBox: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      marginTop: "3%",
      marginBottom: "4%",
    },
    ModalButton: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "5%",
      marginRight: "5%",
      background: theme.palette.secondary.main,
      color: theme.palette.primary.main,
      width: "20%",
      [theme.breakpoints.down('sm')]: {
        width: "40%",
      }
    },
    modalError: {
      fontFamily: theme.typo.fontFamily,
      marginTop: "2.5%",
      marginBottom: "2.5%",
      textAlign: "center"
    },
});