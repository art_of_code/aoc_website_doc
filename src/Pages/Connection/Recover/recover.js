import React from 'react';
import { Dialog, Box, Divider, CircularProgress, withStyles, CssBaseline, FormControl, Button, TextField, Link, Grid, Typography, Container } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { Auth } from 'aws-amplify';

import { styles } from './RecoverStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../../Language/fr.js").default : require("../../../Language/en.js").default;

class Recover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirmpassword: "",
      error: "",
      code: "",
      openModal: false,
      validate: false,
      loader: false,
    }
  }

  componentDidMount() {
    Auth.currentSession()
      .then(() => {
        this.setState({ validate: true });
      })
      .catch((err) => { console.log(err); });
  }

  _onCheckReset = async () => {
    this.setState({ error: "", loader: true });
    if (this.state.email !== "") {
      await Auth.forgotPassword(this.state.email)
        .then(() => {
          this.setState({ openModal: true });
        })
        .catch(err => {
          this.setState({ error: err.message });
        });
    } else {
      this.setState({ error: "Incomplete information" });
    }
    this.setState({ loader: false });
  }

  _onChangePassword = async () => {
    if (this.state.password !== "" && this.state.confirmpassword !== "" && this.state.password === this.state.confirmpassword) {
      await Auth.forgotPasswordSubmit(this.state.email, this.state.code, this.state.password)
        .then(() => {
          this.setState({ validate: true });
        })
        .catch(err => {
          (err.message === "Invalid verification code provided, please try again.") ? this.setState({ error: err }) : this.setState({ error: "Not good password format" });
        });
    } else {
      this.setState({ error: "password not matching" });
    }
  }

  render() {
    const { classes } = this.props;
    if (this.state.validate === true) {
      return <Redirect to='/Login' />
    }
    return (
      <Container component="main" maxWidth="xs" style={{ paddingBottom: '33%' }}>
        <CssBaseline />
        <div className={classes.Page}>
          <Typography component="h1" variant="h5">
            <img src="img/logo.png" alt="logo" />
          </Typography>
          <FormControl className={classes.FormControl} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label={en.recover.email}
              type="email"
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })}
            />
            {(() => {
              switch (this.state.loader) {
                case true: return <CircularProgress className={classes.loader} />;
                default: return null;
              }
            })()}
            <span className={classes.Error}>{this.state.error}</span>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.validateButton}
              onClick={() => this._onCheckReset()}
            >
              {en.recover.recover}
            </Button>
            <Grid container className={classes.rememberAccount}>
              <Grid item xs>
                <Link href="Login" variant="body2">
                  {en.recover.rememberAccount}
                </Link>
              </Grid>
            </Grid>
          </FormControl>
          <Dialog
            open={this.state.openModal}
            onClose={() => { this.setState({ openModal: false }) }}
            keepMounted
            aria-labelledby={en.login.modal.title}
            aria-describedby={en.login.modal.title}
            fullWidth={true}
            maxWidth="md"
          >
            <Box className={classes.Modal}>
              <Typography className={classes.ModalTitle} variant="h5">{en.recover.modal.title}</Typography>
              <Divider variant="middle" />
              <Box className={classes.ModalBox}>
                <Typography className={classes.ModalSubject}>{en.recover.modal.subject}</Typography>
                <span className={classes.modalError}>{this.state.error}</span>
                <TextField className={classes.ModalField} type="text" label={en.recover.modal.code} value={this.state.code} variant="outlined" onChange={e => this.setState({ code: e.target.value })} />
                <TextField className={classes.ModalField} type="password" label={en.recover.modal.newPassword} value={this.state.password} variant="outlined" onChange={e => this.setState({ password: e.target.value })} />
                <TextField className={classes.ModalField} type="password" label={en.recover.modal.confirmPassword} value={this.state.confirmpassword} variant="outlined" onChange={e => this.setState({ confirmpassword: e.target.value })} />
                <Box className={classes.ModalButtonBox}>
                  <Button className={classes.ModalButton} onClick={() => { this.setState({ openModal: false, code: "", password: "", confirmpassword: "" }) }} variant="contained">{en.recover.modal.cancel}</Button>
                  <Button className={classes.ModalButton} onClick={() => { this._onChangePassword(); }} variant="contained">{en.recover.modal.recover}</Button>
                </Box>
              </Box>
            </Box>
          </Dialog>
        </div>
      </Container>
    );
  }
}
export default withStyles(styles)(Recover);