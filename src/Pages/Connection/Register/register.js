import React from 'react';
import { Dialog, Divider, Box, CircularProgress, withStyles, CssBaseline, FormControl, Button, TextField, Link, Grid, Typography, Container } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { Auth } from 'aws-amplify';

import { styles } from './RegisterStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../../Language/fr.js").default : require("../../../Language/en.js").default;

class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      name: "",
      email: "",
      password: "",
      confirmation: "",
      language: (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage,
      error: "",
      code: "",
      openModal: false,
      validate: false,
    }
  }

  componentDidMount() {
    Auth.currentSession()
      .then(() => {
        this.setState({ validate: true });
      })
      .catch((e) => { console.log(e) })
  }

  _onCheckRegister = async () => {
    this.setState({ error: "", loader: true });
    if ((this.state.username !== "") && (this.state.email !== "") && (this.state.name !== "") && (this.state.password !== "") && (this.state.password === this.state.confirmation)) {

      Auth.signUp({
        password: this.state.password,
        username: this.state.email,
        attributes: {
          locale: this.state.language,
          name: this.state.name,
          nickname: this.state.username
        }
      })
        .then(() => this.setState({ openModal: true, loader: false }))
        .catch(err => {
          this.setState({ error: err.message });
        });
    } else {
      this.setState({ error: "Incomplete or wrong informations" });
    }
    this.setState({ loader: false });
  }

  _onSendAgainEmail = async () => {
    Auth.resendSignUp(this.state.email)
      .then(() => {
        this.setState({ error: "code resent successfully" });
      })
      .catch((err) => {
        this.setState({ error: err.message });
      });
  }

  _onCheckEmail = async () => {
    Auth.confirmSignUp(this.state.email, this.state.code)
      .then(() => {
        this.setState({ openModal: false, code: "", validate: true });
      })
      .catch(() => {
        this.setState({ error: "Invalid Code" });
      });
  }

  render() {
    const { classes } = this.props;
    if (this.state.validate === true) {
      return <Redirect to='/Login' />
    }
    return (
      <Container component="main" maxWidth="xs" style={{ paddingBottom: '10%' }}>
        <CssBaseline />
        <div className={classes.Page}>
          <Typography component="h1" variant="h5">
            <img src="img/logo.png" alt="logo" />
          </Typography>
          <FormControl className={classes.FormControl} noValidate>
            <TextField
              margin="normal"
              variant="outlined"
              fullWidth
              label={en.register.username}
              value={this.state.username}
              onChange={e => this.setState({ username: e.target.value })}
            />
            <TextField
              margin="normal"
              variant="outlined"
              fullWidth
              label={en.register.realName}
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
            />
            <TextField
              margin="normal"
              variant="outlined"
              fullWidth
              label={en.register.email}
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })}
            />
            <TextField
              margin="normal"
              variant="outlined"
              fullWidth
              label={en.register.password}
              type="password"
              value={this.state.password}
              onChange={e => this.setState({ password: e.target.value })}
            />
            <TextField
              margin="normal"
              variant="outlined"
              fullWidth
              label={en.register.confirmPassword}
              type="password"
              value={this.state.confirmation}
              onChange={e => this.setState({ confirmation: e.target.value })}
            />
            {(() => {
              switch (this.state.loader) {
                case true: return <CircularProgress className={classes.loader} />;
                default: return null;
              }
            })()}
            <span className={classes.Error}>{this.state.error}</span>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.validateButton}
              onClick={() => this._onCheckRegister()}
            >
              {en.register.signup}
            </Button>
            <Grid container className={classes.goSignIn}>
              <Grid item>
                <Link href="/Login" variant="body2">
                  {en.register.signin}
                </Link>
              </Grid>
            </Grid>
          </FormControl>
          <Dialog
            open={this.state.openModal}
            onClose={() => { this.setState({ openModal: false }) }}
            keepMounted
            aria-labelledby={en.login.modal.title}
            aria-describedby={en.login.modal.title}
            fullWidth={true}
            maxWidth="md"
          >
            <Box className={classes.Modal}>
              <Typography className={classes.ModalTitle} variant="h5">{en.register.modal.title}</Typography>
              <Divider variant="middle" />
              <Box className={classes.ModalBox}>
                <Typography className={classes.ModalSubject}>{en.register.modal.subject}</Typography>
                <span className={classes.modalError}>{this.state.error}</span>
                <TextField className={classes.ModalField} type="text" label={en.register.modal.field} value={this.state.code} variant="outlined" onChange={e => this.setState({ code: e.target.value })} />
                <Box className={classes.ModalButtonBox}>
                  <Button className={classes.ModalButton} onClick={() => { this._onSendAgainEmail(); }} variant="contained">{en.register.modal.resend}</Button>
                  <Button className={classes.ModalButton} onClick={() => { this._onCheckEmail(); }} variant="contained">{en.register.modal.verify}</Button>
                </Box>
              </Box>
            </Box>
          </Dialog>
        </div>
      </Container>
    );
  }
}
export default withStyles(styles)(Register);