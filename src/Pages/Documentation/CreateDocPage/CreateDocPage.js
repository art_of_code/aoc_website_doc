import React from "react";
import { withStyles } from '@material-ui/core/styles';
import { Container, Fab, TextField, Button } from '@material-ui/core'
import { Add as AddIcon } from '@material-ui/icons'
import MdEditor from 'react-markdown-editor-lite'
import MarkdownIt from 'markdown-it'
import { Auth } from 'aws-amplify';
import { API_URL } from '../../../App.js';

import { styles } from './createDocPageStyles';

class CreateDocPage extends React.Component {
  mdParser = null;
  constructor(props) {
    super(props);
    this.mdParser = new MarkdownIt(/* Markdown-it options */)
    this.state = {
      validate: false,
      idIndex: 1,
      result: {
        title: "",
        content: "",
        elements: [],
        id: 0
      }
    }

    this._createPage = this._createPage.bind(this);
    this._generatePart = this._generatePart.bind(this);
    this._generateSubPart = this._generateSubPart.bind(this);
    this._addPart = this._addPart.bind(this);
    this._changeTitle = this._changeTitle.bind(this);
    this._changeContent = this._changeContent.bind(this);
    this._saveDoc = this._saveDoc.bind(this);
  }

  _saveDoc = async () => {
    this.setState({ error: "" });
    let session = await Auth.currentSession();
    await fetch(API_URL + '/documentation/article', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${session.idToken.jwtToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "title": this.state.result.title,
        "content": this.state.result.content,
        "elements": this.state.result.elements
      })
    })
      .then(res => res.json())
      .then((res) => {
        if (res.error) {
          console.log("ERROR");
        } else {
          console.log(res);
          this.setState({ validate: true });
        }
      })
      .catch((e) => {
        // FAILURE
        console.log("ERROR");
        console.log(e);
      })
  }

  _addPart(childOf) {
    let result = this.state.result;

    console.log(childOf);
    if (childOf === result.id) {
      result.elements.push({
        title: "",
        content: "",
        elements: [],
        id: this.state.idIndex
      });
      console.log(this.state.idIndex);
      this.setState({ idIndex: this.state.idIndex + 1 });
    }
    for (let i = 0; i < result.elements.length; i++) {
      if (childOf === result.elements[i].id) {
        result.elements[i].elements.push({
          title: "",
          content: "",
          elements: [],
          id: this.state.idIndex
        });
        this.setState({ idIndex: this.state.idIndex + 1 });
      }
    }
    this.setState({ result: result })
    console.log(this.state.result);
  }

  _changeContent(id, value) {
    console.log(value);
    let result = this.state.result;
    if (id === result.id) {
      result.content = value;
    }
    for (let i = 0; i < result.elements.length; i++) {
      if (id === result.elements[i].id) {
        result.elements[i].content = value;
      }
      for (let j = 0; j < result.elements[i].elements.length; j++) {
        if (id === result.elements[i].elements[j].id) {
          result.elements[i].elements[j].content = value;
        }
      }
    }
    this.setState({ result: result })
  }

  _generateSubPart(elements) {
    let result = [];

    for (let elem of elements) {
      result.push(<TextField
        variant="outlined"
        margin="normal"
        fullWidth
        label="Title"
        type="text"
        id={elem.id}
        value={elem.title}
        onChange={e => this._changeTitle(elem.id, e.target.value)}
      />);
      result.push(<div>
        <MdEditor
          value={elem.content}
          renderHTML={(text) => this.mdParser.render(text)}
          config={{
            view: {
              menu: true,
              md: true,
              html: true,
              fullScreen: false
            },
          }}
          onChange={text => this._changeContent(elem.id, text.text)}
        />
      </div>)
    }
    return <div style={{ 'margin-left': '10%', 'marginBottom': '2%' }}>{result}</div>;
  }

  _generatePart(elements) {
    let result = [];

    for (let elem of elements) {
      result.push(<TextField
        variant="outlined"
        margin="normal"
        fullWidth
        label="Title"
        type="text"
        id={elem.id}
        value={elem.title}
        onChange={e => this._changeTitle(elem.id, e.target.value)}
      />);
      result.push(<div>
        <MdEditor
          value={elem.content}
          renderHTML={(text) => this.mdParser.render(text)}
          config={{
            view: {
              menu: true,
              md: true,
              html: true,
              fullScreen: false
            },
          }}
          onChange={text => this._changeContent(elem.id, text.text)}
        />
      </div>)
      result.push(this._generateSubPart(elem.elements));
      result.push(<Fab color="primary" aria-label="add" childOf="id" onClick={e => this._addPart(elem.id)}>
        <AddIcon />
      </Fab >);
    }
    return <div style={{ 'margin-left': '10%', 'marginBottom': '2%' }}>{result}</div>;
  }

  _changeTitle(id, value) {
    console.log(id);
    let result = this.state.result;
    if (id === result.id) {
      result.title = value;
    }
    for (let i = 0; i < result.elements.length; i++) {
      if (id === result.elements[i].id) {
        result.elements[i].title = value;
      }
      for (let j = 0; j < result.elements[i].elements.length; j++) {
        if (id === result.elements[i].elements[j].id) {
          result.elements[i].elements[j].title = value;
        }
      }
    }
    this.setState({ result: result })
  }

  _createPage() {
    let result = [];
    // let index = 0;

    result.push(<TextField
      variant="outlined"
      margin="normal"
      fullWidth
      label="Title"
      type="text"
      value={this.state.result.title}
      onChange={e => this._changeTitle(this.state.result.id, e.target.value)}
    />);
    result.push(<div>
      <MdEditor
        value={this.state.result.content}
        renderHTML={(text) => this.mdParser.render(text)}
        config={{
          view: {
            menu: true,
            md: true,
            html: true,
            fullScreen: false
          },
        }}
        onChange={text => this._changeContent(this.state.result.id, text.text)}
      />
    </div>)
    result.push(this._generatePart(this.state.result.elements));
    result.push(<Fab style={{ 'marginBottom': '5%', 'marginUp': '2%' }} color="primary" aria-label="add" childOf={this.state.result.id} onClick={e => this._addPart(this.state.result.id)}>
      <AddIcon />
    </Fab>);

    return <div style={{ 'margin-bottom': '10%' }}>{result}</div>
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.Page}>
        <Container component="main">
          <Button variant="contained" color="primary" onClick={this._saveDoc} className={classes.SaveButton}>
            Save
          </Button>
          {this._createPage()}
        </Container>
      </div>
    );
  }
}

export default withStyles(styles)(CreateDocPage);