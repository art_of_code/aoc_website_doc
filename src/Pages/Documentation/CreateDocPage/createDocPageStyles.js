export const styles = theme => ({
    Page: {
      marginTop: "4em",
      marginLeft: "13%",
    },
    Internal: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "20%"
    },
    SaveButton: {
      fontFamily: theme.typo.fontFamily,
      float: "right",
      marginTop: "5%"
    }
});