import React from "react";
import { withStyles } from '@material-ui/core/styles';
import { CardContent, Grid, Typography, CircularProgress, Box } from '@material-ui/core'
import { API_URL } from '../../../App.js';
import { Card } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

import { styles } from './docListStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../../Language/fr.js").default : require("../../../Language/en.js").default;

class DocList extends React.Component {
  constructor(props) {
    super(props);
    this._getList();
    this.state = {
      list: [],
      search: "",
      loader: true,
    }
    this._renderList = this._renderList.bind(this);
  }

  _getList = async () => {
    await fetch(API_URL + 'documentation/article/', {
      method: 'GET'
    }).then(res => res.json())
      .then((res) => {
        if (res.error) {
          console.log("ERROR");
        } else {
          this.setState({ list: res, loader: false })
        }
      })
      .catch((e) => {
        console.log("ERROR: " + e)
      })
  }

  _renderList() {
    const { classes } = this.props;
    let result = [];

    for (let item of this.state.list) {
      if (item.title.toLowerCase().indexOf(this.state.search) !== -1 || item.content.toLowerCase().indexOf(this.state.search) !== -1) {
        result.push(
          <Grid item xs={12} sm={3} key={item.title}>
            <Card className={classes.Card} onClick={() => { this.props.history.push("/DocumentationPage/" + item._id) }}>
              <CardContent>
                <Typography className={classes.title} color="textPrimary">{item.title}</Typography>
                <Typography className={classes.cardContent} color="textSecondary">{item.content}</Typography>
              </CardContent>
            </Card>
          </Grid>
        )
      }
    }
    return (
      <Grid container className={classes.grid} spacing={3}>
        {result}
      </Grid>
    )
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.Page}>
        <div className={classes.searchBar}>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon style={{ color: "#F4F4F4" }} />
            </div>
            <InputBase
              placeholder={en.docList.search}
              onChange={e => this.setState({ search: e.target.value })}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
        </div>
        {(() => {
          switch (this.state.loader) {
            case true: return <Box className={classes.boxLoader}><CircularProgress className={classes.loader} /></Box>;
            default: return null;
          }
        })()}
        {this._renderList()}
      </div>
    );
  }
}

export default withStyles(styles)(DocList);