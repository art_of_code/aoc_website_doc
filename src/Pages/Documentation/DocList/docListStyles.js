import { fade } from '@material-ui/core/styles';

export const styles = theme => ({
    Page: {
      marginTop: "3.5em",
      background: theme.palette.fourth.main,
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      }
    },
    Card: {
      height: 170,
      cursor: "pointer",
    },
    root: {
      flexGrow: 1
    },
    grid: {
      width: "80vw",
      minHeight: "50vw",
      marginLeft: "10%",
      paddingTop: "5%",
      alignContent: "flex-start",
      alignItems: "center",
    },
    title: {
      fontFamily: theme.typo.fontFamily,
      fontSize: 17,
      "text-transform": "capitalize",
    },
    cardContent: {
      fontFamily: theme.typo.fontFamily,
    },
    searchBar: {
      fontFamily: theme.typo.fontFamily,
      width: "auto",
      backgroundColor: theme.palette.primary.main,
      padding: "1% 0 1% 1%",
      [theme.breakpoints.down('xs')]: {
        padding: "1% 0 1% 2%",
      },
    },
    search: {
      fontFamily: theme.typo.fontFamily,
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: 'auto',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      fontFamily: theme.typo.fontFamily,
      color: 'white',
      width: "100%",
    },
    inputInput: {
      fontFamily: theme.typo.fontFamily,
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
    },
    boxLoader: {
      display: 'flex',
      paddingTop: '5%',
    },
    loader: {
      margin: 'auto',
      color: theme.palette.primary.main,
    },
});