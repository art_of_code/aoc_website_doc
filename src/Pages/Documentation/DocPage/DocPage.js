import React from "react";
import ReactDOM from 'react-dom';
import { withStyles } from '@material-ui/core/styles';
import { Container, Fab, TextField, Button, Tooltip, Box } from '@material-ui/core';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { Add as AddIcon } from '@material-ui/icons';
import MdEditor from 'react-markdown-editor-lite';
import MarkdownIt from 'markdown-it';
import hljs from 'highlight.js';
import { Auth } from 'aws-amplify';
import { API_URL } from '../../../App.js';
import { Redirect } from 'react-router-dom';

import { styles } from './docPageStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../../Language/fr.js").default : require("../../../Language/en.js").default;

var md = new MarkdownIt({
  highlight: function (str, lang) {
    if (lang && hljs.getLanguage(lang)) {
      try {
        return '<pre class="hljs"><code>' +
          hljs.highlight(lang, str, true).value +
          '</code></pre>';
      } catch (__) { }
    }

    return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
  }
})

class DocPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validate: false,
      edit: false,
      page: {},
      idIndex: 1,
      maxIndex: 0,
    }
    this._getPage();
    this._manageParts = this._manageParts.bind(this);
    this._manageSubParts = this._manageSubParts.bind(this);
    this._renderDoc = this._renderDoc.bind(this);
    this._createPage = this._createPage.bind(this);
    this._generatePart = this._generatePart.bind(this);
    this._generateSubPart = this._generateSubPart.bind(this);
    this._addPart = this._addPart.bind(this);
    this._changeTitle = this._changeTitle.bind(this);
    this._changeContent = this._changeContent.bind(this);
    this._saveDoc = this._saveDoc.bind(this);
    this.changeEdit = this.changeEdit.bind(this);
  }

  componentDidUpdate() {
    let hash = this.props.location.hash.replace('#', '');
    if (hash) {
      let node = ReactDOM.findDOMNode(this.refs[hash]);
      if (node) {
        node.scrollIntoView();
      }
    }
  }

  componentDidMount() {
    Auth.currentSession()
      .catch(() => this.setState({ redirect: true }))
  }

  changeEdit() {
    if (this.state.edit) {
      this._saveDoc();
    } else {
      this.setState({ edit: true });
    }
  }

  _saveDoc = async () => {
    this.setState({ error: "" });
    let session = await Auth.currentSession();
    await fetch(API_URL + 'documentation/article/', {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${session.idToken.jwtToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "id": this.props.match.params.pageId,
        "title": this.state.page.title,
        "content": this.state.page.content,
        "elements": this.state.page.elements
      })
    })
      .then(res => res.json())
      .then((res) => {
        if (res.error) {
          console.log("ERROR");
        } else {
          console.log(res);
          this.setState({ validate: true });
        }
      })
      .catch((e) => {
        // FAILURE
        console.log("ERROR: "+e);
      })
  }

  _addPart(childOf) {
    let result = this.state.page;

    if (childOf === result.id) {
      result.elements.push({
        title: "",
        content: "",
        elements: [],
        id: this.state.idIndex
      });
      this.setState({ idIndex: this.state.idIndex + 1 });
    }
    for (let i = 0; i < result.elements.length; i++) {
      if (childOf === result.elements[i].id) {
        result.elements[i].elements.push({
          title: "",
          content: "",
          elements: [],
          id: this.state.idIndex
        });
        this.setState({ idIndex: this.state.idIndex + 1 });
      }
    }
    this.setState({ page: result })
  }

  _changeContent(id, value) {
    let result = this.state.page;
    if (id === result.id) {
      result.content = value;
    }
    for (let i = 0; i < result.elements.length; i++) {
      if (id === result.elements[i].id) {
        result.elements[i].content = value;
      }
      for (let j = 0; j < result.elements[i].elements.length; j++) {
        if (id === result.elements[i].elements[j].id) {
          result.elements[i].elements[j].content = value;
        }
      }
    }
    this.setState({ page: result })
  }

  _generateSubPart(elements) {
    let result = [];

    for (let elem of elements) {
      result.push(<TextField
        variant="outlined"
        margin="normal"
        fullWidth
        label="Title"
        type="text"
        id={elem.id}
        value={elem.title}
        onChange={e => this._changeTitle(elem.id, e.target.value)}
      />);
      result.push(<div>
        <MdEditor
          value={elem.content}
          renderHTML={(text) => md.render(text)}
          config={{
            view: {
              menu: true,
              md: true,
              html: true,
              fullScreen: false
            },
          }}
          onChange={text => this._changeContent(elem.id, text.text)}
        />
      </div>)
    }
    return <div style={{ 'marginLeft': '10%', 'marginBottom': '2%' }}>{result}</div>;
  }

  _generatePart(elements) {
    let result = [];

    for (let elem of elements) {
      result.push(<TextField
        variant="outlined"
        margin="normal"
        fullWidth
        label="Title"
        type="text"
        id={elem.id.toString()}
        value={elem.title}
        onChange={e => this._changeTitle(elem.id, e.target.value)}
      />);
      result.push(<div>
        <MdEditor
          value={elem.content}
          renderHTML={(text) => md.render(text)}
          config={{
            view: {
              menu: true,
              md: true,
              html: true,
              fullScreen: false
            },
          }}
          onChange={text => this._changeContent(elem.id, text.text)}
        />
      </div>)
      result.push(this._generateSubPart(elem.elements));
      result.push(<Fab color="primary" aria-label="add" childof="id" onClick={e => this._addPart(elem.id)}>
        <AddIcon />
      </Fab >);
    }
    return <div style={{ 'marginLeft': '10%', 'marginBottom': '2%' }}>{result}</div>;
  }

  _changeTitle(id, value) {
    let result = this.state.page;
    if (id === result.id) {
      result.title = value;
    }
    for (let i = 0; i < result.elements.length; i++) {
      if (id === result.elements[i].id) {
        result.elements[i].title = value;
      }
      for (let j = 0; j < result.elements[i].elements.length; j++) {
        if (id === result.elements[i].elements[j].id) {
          result.elements[i].elements[j].title = value;
        }
      }
    }
    this.setState({ page: result })
  }

  _createPage() {
    let result = [];

    result.push(<TextField
      variant="outlined"
      margin="normal"
      fullWidth
      label="Title"
      type="text"
      value={this.state.page.title}
      onChange={e => this._changeTitle(this.state.page.id, e.target.value)}
    />);
    result.push(<div>
      <MdEditor
        value={this.state.page.content}
        renderHTML={(text) => md.render(text)}
        config={{
          view: {
            menu: true,
            md: true,
            html: true,
            fullScreen: false
          },
        }}
        onChange={text => this._changeContent(this.state.page.id, text.text)}
      />
    </div>)
    result.push(this._generatePart(this.state.page.elements));
    result.push(<Fab style={{ 'marginBottom': '5%', 'marginUp': '2%' }} color="primary" aria-label="add" childof={this.state.page.id} onClick={e => this._addPart(this.state.page.id)}>
      <AddIcon />
    </Fab>);

    return <div style={{ 'marginBottom': '10%' }}>{result}</div>
  }


  _getPage = async () => {
    await fetch(API_URL + '/documentation/article/' + this.props.match.params.pageId, {
      method: 'GET'
    }).then(res => res.json())
      .then((res) => {
        if (res.error) {
          console.log("ERROR");
        } else {
          this.setState({ page: res });
        }
      })
      .catch((e) => {
        console.log("ERROR: "+e);
      })
  }

  _manageSubParts(elements) {
    let result = [];

    for (let elem of elements) {
      if (elem.id >= this.state.idIndex) {
        this.setState({ idIndex: elem.id + 1 })
      }
      result.push(<h3 ref={elem.title}>{elem.title}</h3>);
      if (elem.content) {
        result.push(<div dangerouslySetInnerHTML={{ __html: md.render(elem.content) }}></div>);
      }
    }
    return <div style={{ marginLeft: '1%' }}>{result}</div>;
  }

  _manageParts(elements) {
    let result = [];

    for (let elem of elements) {
      if (elem.id >= this.state.idIndex) {
        this.setState({ idIndex: elem.id + 1 })
      }
      result.push(<h2 ref={elem.title}>{elem.title}</h2>);
      if (elem.content) {
        result.push(<div dangerouslySetInnerHTML={{ __html: md.render(elem.content) }}></div>);
      }
      if (elem.elements) {
        result.push(this._manageSubParts(elem.elements));
      }
    }
    return <div style={{ marginLeft: '1%' }}>{result}</div>;
  }

  _renderDoc() {
    let result = [];

    result.push(<h1>{this.state.page.title}</h1>);
    if (this.state.page.content) {
      result.push(<div dangerouslySetInnerHTML={{ __html: md.render(this.state.page.content) }}></div>);
    }
    if (this.state.page.elements && this.state.page.elements.length > 0) {
      result.push(this._manageParts(this.state.page.elements));
    }
    return <div>{result}</div>
  }

  render() {
    const { classes } = this.props;
    if (this.state.validate === true) {
      return <Redirect to="/DocumentationList" />
    }
    return (
      <div className={classes.Page}>
        <Container component="main">
          <Box className={classes.responseButton}>
            <Box className={classes.responseBackBox}>
              <Tooltip title={en.docPage.goBack} aria-label={en.docPage.goBack}>
                <Button onClick={() => { this.props.history.push("/DocumentationList/") }} ><KeyboardBackspaceIcon />{en.docPage.back}</Button>
              </Tooltip>
            </Box>
            <Box className={classes.responseSaveBox}>
              <Tooltip title={en.docPage.goEdit} aria-label={en.docPage.goEdit}>
                <Button onClick={this.changeEdit}>{this.state.edit ? en.docPage.save : en.docPage.edit}</Button>
              </Tooltip>
            </Box>
          </Box>
        </Container>
        <Container component="main" className={classes.container}>{ this.state.edit ? this._createPage() : this._renderDoc() }</Container>
      </div>
    );
  }
}

export default withStyles(styles)(DocPage);