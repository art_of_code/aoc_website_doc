export const styles = theme => ({
    Page: {
      marginTop: "3.5em",
      minHeight: "45em",
      background: theme.palette.fourth.main,
      paddingTop: "2%",
      paddingBottom: "5%",
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      },
    },
    container: {
      paddingTop: "1%",
      paddingBottom: "2%",
      background: "white",
      borderRadius: "0.5%",
      boxShadow: "3px 3px 15px -7px rgba(0,0,0,0.75)",
      [theme.breakpoints.down('sm')]: {
        background: theme.palette.fourth.main,
        boxShadow: "none",
      },
    },
    responseSaveBox: {
      width: "25%",
      display: "flex",
      justifyContent: "flex-end",
      margin: "auto",
    },
    responseBackBox: {
      width: "100%",
      display: "flex",
      justifyContent: "flex",
      margin: "auto",
    },
    responseButton: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      margin: "auto",
      marginBottom: "1em",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      [theme.breakpoints.down('xs')]: {
        width: "100%",
      },
    },
});