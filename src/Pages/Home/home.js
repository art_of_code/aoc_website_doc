import React from "react";
import { Button, Paper, Table, TableBody, TableHead, TableRow, TableCell, TableContainer, withStyles, Box, Card, CardContent, Typography } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { styles } from "./homeStyles";

import { motion } from "framer-motion";

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trad: "",
      index: 1,
    }
    this.values = React.createRef()
  }

  handleOnClick = (event) => {
    if(this.values.current){
        this.values.current.scrollIntoView({ 
           behavior: "smooth", 
           block: "end"
        })
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.Page}>
        <Box className={classes.firstPage}>
          <img src="https://aoc-backoffice.s3.eu-west-2.amazonaws.com/gif.gif" alt="gif background" className={classes.gifBackground}></img>
          <img src="img/logo.png" alt="logo" className={classes.logo}></img>
          <div className={classes.expandMore}>
          <motion.div
            animate={{
              scale: [1, 1.5, 1.5, 1, 1],
              y: [0, 30, 30, 0, 0],
            }}
            transition={{
              duration: 2,
              ease: "easeInOut",
              times: [0, 0.2, 0.5, 0.8, 1],
              loop: Infinity,
              repeatDelay: 1
            }}
            style={{ cursor: "pointer" }}
            onClick={this.handleOnClick}
          >
            <ExpandMoreIcon style={{ fontSize: 60 }}/>
          </motion.div>
          </div>
        </Box>
        <Box ref={this.values} className={classes.thirdPage}>
          <Card className={classes.thirdCard}>
            <CardContent>
              <Typography className={classes.CardTitle} variant="h4">
                {en.homePage.valueTitle}
              </Typography>
              <Typography className={classes.CardText} variant="h5">
                {en.homePage.valueText}
              </Typography>
            </CardContent>
          </Card>
        </Box>
        <Box className={classes.secondPage}>
          <Box className={classes.secondPageLeft}>
            <Typography className={classes.CardTitleFunc}variant="h4">
              {en.homePage.tableTitle}
            </Typography>
            <Typography className={classes.CardTextFunc} variant="h5">
              {en.homePage.tableSecondTitle}
            </Typography>
            {/* Ajouter le tableau */}
            <TableContainer component={Paper}>
              <Table aria-label="benefits table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.benefits}</TableCell>
                    <TableCell align="center">{en.homePage.table.community}</TableCell>
                    <TableCell align="center">{en.homePage.table.professional}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.onlineDoc}</TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.software}</TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.stats}</TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.advanceStats}</TableCell>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.monthlyReport}</TableCell>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.team}</TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">{en.homePage.table.companyTeam}</TableCell>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"><CheckIcon /></TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <Box className={classes.secondPageLeftButtonBox}>
              <Button
                type="submit"
                fullWidth
                className={classes.secondPageLeftButtonLeft}
                onClick={() => { window.location.href = '/Analyser' }}
              >
                {en.homePage.freeButton}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                className={classes.secondPageLeftButton}
                onClick={() => { window.open('https://artofcode.eu/', '_blank') }}
              >
                {en.homePage.proButton}
              </Button>
            </Box>
          </Box>
          <Box className={classes.secondPageRight}>
            {/* Ajouter le Logo */}
            <motion.div
              whileHover={{ scale: 1.1 }}
              whileTap={{ scale: 0.9 }}
            >
              <img src="img/logo.png" alt="logo" className={classes.logo}></img>
            </motion.div>
          </Box>
        </Box>
        <Box className={classes.fourthPage}>
          <motion.div
            whileHover={{ scale: 1.1 }}
            className={classes.fourthCard}
          >
            <Card>
              <CardContent>
                <Typography className={classes.CardTitle} variant="h4">{en.homePage.card1Title}</Typography>
                <Typography className={classes.CardTextValue} variant="h5">{en.homePage.card1Text}</Typography>
              </CardContent>
            </Card>
          </motion.div>
          <motion.div
            whileHover={{ scale: 1.1 }}
            className={classes.fourthCard}
          >
            <Card>
              <CardContent>
                <Typography className={classes.CardTitle} variant="h4">{en.homePage.card2Title}</Typography>
                <Typography className={classes.CardTextValue} variant="h5">{en.homePage.card2Text}</Typography>
              </CardContent>
            </Card>
          </motion.div>
          <motion.div
            whileHover={{ scale: 1.1 }}
            className={classes.fourthCard}
          >
            <Card>
              <CardContent>
                <Typography className={classes.CardTitle} variant="h4">{en.homePage.card3Title}</Typography>
                <Typography className={classes.CardTextValue} variant="h5">{en.homePage.card3Text}</Typography>
              </CardContent>
            </Card>
          </motion.div>
        </Box>
        {/* End code page here */}
      </div>
    );
  }
}
export default withStyles(styles)(Home);