export const styles = theme => ({
    Page: {
        marginTop: "3.5em",
        background: theme.palette.fourth.main,
        [theme.breakpoints.up('sm')]: {
          marginTop: "4em",
        }
      },
      firstPage: {
        width: "100%",
        height: "50em",
        margin: "auto",
      },
      gifBackground: {
        position: "absolute",
        width: "100%",
        height: "50em",
        objectFit: "cover",
        filter: "brightness(0.3)",
        [theme.breakpoints.down('sm')]: {
          height: "45em",
        }
      },
      logo: {
        position: "relative",
        display: "block",
        margin: "auto",
        paddingTop: "10%",
      },
      expandMore: {
        color: "white",
        position: "relative",
        display: "flex",
        justifyContent: "center",
        paddingTop: "8%",
        [theme.breakpoints.down('xs')]: {
          paddingTop: "35%",
        }
      },
      secondPage: {
        width: "100%",
        height: "50em",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        textAlign: "center",
        paddingBottom: "10%",
        [theme.breakpoints.down('md')]: {
          flexDirection: "column",
          height: "auto",
          paddingBottom: "0",
        },
      },
      secondPageLeft: {
        marginLeft: "10%",
        marginTop: "7%",
        marginRight: "13%",
        [theme.breakpoints.down('md')]: {
          marginLeft: "1%",
          marginRight: "1%",
          marginTop: "3%",
        }
      },
      secondPageLeftButton: {
        background: theme.palette.primary.main,
        color: theme.palette.font1.color,
        margin: "1em 2em",
        padding: "0.5em",
        [theme.breakpoints.down('md')]: {
          margin: "1em 1em",
        }
      },
      secondPageLeftButtonLeft: {
        color: theme.palette.primary.main,
        margin: "1em 2em",
        padding: "0.5em",
        border: "2px solid"+theme.palette.primary.main,
        [theme.breakpoints.down('md')]: {
          margin: "1em 1em",
        }
      },
      secondPageLeftButtonBox: {
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
      },
      secondPageRight: {
        marginTop: "14%",
        marginRight: "15%",
        [theme.breakpoints.down('md')]: {
          marginRight: 0,
          marginTop: 0,
          marginBottom: "10%",
        }
      },
      thirdPage: {
        width: "100%",
        height: "25em",
        paddingTop: "10%",
        [theme.breakpoints.down('md')]: {
          height: "auto",
          marginBottom: "10%",
          paddingTop: 0,
        }
      },
      thirdCard: {
        marginLeft: "20%",
        marginRight: "20%",
        background: "white",
        display: "flex",
        justifyContent: "center",
        [theme.breakpoints.down('md')]: {
          marginLeft: "1%",
          marginRight: "1%",
        }
      },
      CardTitle: {
        paddingTop: "2%",
        paddingBottom: "3%",
        margin: "0 2% 0 2%",
        fontFamily: theme.typo.fontFamily,
        [theme.breakpoints.down('md')]: {
          paddingTop: 0,
          paddingBottom: 0,
        }
      },
      CardTextValue: {
        minHeight: "5.5em",
        fontFamily: theme.typo.fontFamily,
        color: theme.palette.font2.color,
        margin: "3% 3% 0 3%",
      },
      CardText: {
        fontFamily: theme.typo.fontFamily,
        color: theme.palette.font2.color,
        textAlign: "justify",
        margin: "0 3% 0 3%",
      },
      CardTitleFunc: {
        fontFamily: theme.typo.fontFamily,
        fontWeight: "bold",
        color: theme.palette.font2.color,
        justifyContent: "center",
        textAlign: "center",
        marginBottom: "2%",
      },
      CardTextFunc: {
        fontFamily: theme.typo.fontFamily,
        color: theme.palette.font2.color,
        display: "flex",
        justifyContent: "center",
        textAlign: "center",
        marginBottom: "2%",
        width: "80%",
        margin: "0 10% 4% 10%",
        [theme.breakpoints.down('xs')]: {
          width: "90%",
          margin: "0 5% 4% 5%",
        },
      },

      fourthPage: {
        width: "100%",
        height: "auto",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        textAlign: "center",
        [theme.breakpoints.down('md')]: {
          flexDirection: "column",
          height: "auto",
        },
      },
      fourthCard: {
        width: "25%",
        margin: "4% 2% 10% 2%",
        background: "white",
        [theme.breakpoints.down('md')]: {
          width: "95%",
          margin: "auto auto 5% auto",
        },
      },
    });