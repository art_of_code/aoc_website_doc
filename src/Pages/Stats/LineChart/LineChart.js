import React, { Component } from 'react';
import * as d3 from "d3";
import moment from "moment";

class LineChart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      series: [],
    }
    this.createPieChart = this.createPieChart.bind(this)
  }

  componentDidUpdate() {
    if (this.props.data[0])
      this.createPieChart();
  }

  compareDates(date1, date2) {
    if (moment(date1).isSame(date2, this.props.temporality))
      return true;
    return false;
  }

  parse(data) {
    let res = [];
    data.forEach(element => {
      let index = res.findIndex(d => this.compareDates(d.date, element.date))

      if (index !== -1) {
        res[index].tests += 1
      } else {
        res.push({ date: moment(element.date).format("YYYY-MM-DD"), tests: 1 });
      }
    });
    res.columns = ["date", "tests"]
    return res;
  }

  createPieChart() {
    const node = this.node;
    let data = this.props.data;
    data = this.parse(data);
    data = data.sort((d, e) => d.date > e.date);
    const height = this.props.height;
    const width = this.props.width;

    let minDate = d3.min(data, d => { return d.date });
    let maxDate = d3.max(data, d => { return d.date });

    let margin = ({ top: 30, right: 50, bottom: 30, left: 30 });
    let series = data.columns.slice(1).map(key => data.map(({ [key]: value, date }) => ({ key, date, value })))


    let x = d3.scaleUtc()
      .domain([new Date(minDate), new Date(maxDate)])
      .range([margin.left, width - margin.right])

    let y = d3.scaleLinear()
      .domain([0, d3.max(series, s => d3.max(s, d => d.value))])
      .range([height - margin.bottom, margin.top])

    let z = d3.scaleOrdinal(data.columns.slice(1), d3.schemeCategory10)

    let xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x).ticks(width / 50).tickSizeOuter(0))

    const svg = d3.select(node).append("svg")
      .attr("viewBox", [0, 0, width, height]);

    svg.append("g")
      .call(xAxis);

    const serie = svg.append("g")
      .selectAll("g")
      .data(series)
      .join("g");

    serie.append("path")
      .attr("fill", "none")
      .attr("stroke", d => z(d[0].key))
      .attr("stroke-width", 1.5)
      .attr("d", d3.line()
        .x(d => {
          return x(new Date(d.date))
        })
        .y(d => y(d.value)));

    serie.append("g")
      .attr("font-family", "sans-serif")
      .attr("font-size", 10)
      .attr("stroke-linecap", "round")
      .attr("stroke-linejoin", "round")
      .attr("text-anchor", "middle")
      .selectAll("text")
      .data(d => d)
      .join("text")
      .text(d => d.value)
      .attr("dy", "0.35em")
      .attr("x", d => x(new Date(d.date)))
      .attr("y", d => y(d.value))
      .call(text => text.filter((d, i, data) => i === data.length - 1)
        .append("tspan")
        .attr("font-weight", "bold")
        .text(d => ` ${d.key}`))
      .clone(true).lower()
      .attr("fill", "none")
      .attr("stroke", "white")
      .attr("stroke-width", 6);


  }

  render() {
    return <svg style={{ width: "100%" }} ref={node => this.node = node}
      width={this.props.width} height={this.props.height}>
    </svg>
  }
}
export default LineChart
