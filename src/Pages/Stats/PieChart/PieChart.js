import React, { Component } from 'react';
import * as d3 from "d3";

class PieChart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      series: [],
    }
    this.createPieChart = this.createPieChart.bind(this)
  }

  componentDidUpdate() {
    if (this.props.data)
      this.createPieChart();
  }

  parse(data) {
    let res = this.props.columns.map(e => {
      return { name: e, value: 0 };
    });

    for (let elem of data) {
      let index = res.findIndex(e => e.name === elem.name);
      res[index].value++;
    }
    return res;
  }

  createPieChart() {
    const node = this.node;
    const width = this.props.width;
    const height = this.props.height;
    let color = this.props.color;

    let data = this.props.data;
    data = this.parse(data);

    let pie = d3.pie()
      .sort(null)
      .value(d => d.value)
    const arcs = pie(data);
    const svg = d3.select(node)
      .append("svg")
      .attr("viewBox", [-width / 2, -height / 2, width, height]);

    let arc = d3.arc()
      .innerRadius(0)
      .outerRadius(Math.min(width, height) / 2 - 1)

    svg.append("g")
      .attr("stroke", "white")
      .selectAll("path")
      .data(arcs)
      .join("path")
      .attr("fill", d => color(d.data.name))
      .attr("d", arc)
      .append("title")
      .text(d => `${d.data.name}: ${d.data.value.toLocaleString()}`);

    svg.append("g")
      .attr("font-family", "sans-serif")
      .attr("font-size", 12)
      .attr("text-anchor", "middle")
      .selectAll("text")
      .data(arcs)
      .join("text")
      .attr("transform", d => {
        const radius = Math.min(width, height) / 2 * 0.8;
        let tmp = d3.arc().innerRadius(radius).outerRadius(radius);
        return `translate(${tmp.centroid(d)})`
      })
      .call(text => text.append("tspan")
        .attr("y", "-0.4em")
        .attr("font-weight", "bold")
        .text(d => d.data.name))
      .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
        .attr("x", 0)
        .attr("y", "0.7em")
        .attr("fill-opacity", 0.7)
        .text(d => d.data.value.toLocaleString()));

  }

  render() {
    return <svg style={{ width: "100%", margin: "auto" }} ref={node => this.node = node}
      width={this.props.width} height={this.props.height}>
    </svg>
  }
}
export default PieChart