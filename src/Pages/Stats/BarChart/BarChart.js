import React, { Component } from 'react';
import * as d3 from "d3";
import moment from "moment";

class BarChart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      series: [],
    }
    this.createBarChart = this.createBarChart.bind(this)
  }

  componentDidUpdate() {
    if (this.props.data)
      this.createBarChart()
  }

  compareDates(date1, date2) {
    if (moment(date1).isSame(date2, this.props.temporality))
      return true;
    return false;
  }

  parse(oui) {
    let res = [];
    let columns = [];
    let data = JSON.parse(JSON.stringify(oui));
    columns.push("date");
    columns = columns.concat(this.props.columns);

    data.forEach(element => {
      let index = res.findIndex(d => this.compareDates(element.date, d.date));

      if (index !== -1) {
        res[index][element.name] += 1;
      } else {
        let tmp = {};
        for (let elem of columns) {
          tmp[elem] = 0;
        }
        tmp.date = element.date;
        tmp[element.name] += 1;
        res.push(tmp);
      }
    });
    res["columns"] = columns;
    return res;
  }

  createReturn() {
    return <svg style={{ width: "100%", margin: "auto" }} ref={node => this.node = node}
      width={this.props.width} height={this.props.height - 200}>
    </svg>;
  }



  createBarChart() {
    const node = this.node;
    d3.selectAll("svg > *").remove();
    let data = this.props.data;
    data = this.parse(data);

    data = data.sort((d, e) => d.date > e.date);

    let height = this.props.height;
    let width = this.props.width;
    let margin = ({ top: 10, right: 10, bottom: 20, left: 40 });

    let groupKey = data.columns[0]
    let keys = data.columns.slice(1)

    const svg = d3.select(node)
      .classed("svg-container", true)
      .append("svg")
      // Responsive SVG needs these 2 attributes and no width and height attr.
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 600 600")
      // Class to make it responsive.
      .classed("svg-content-responsive", true)

    let color = this.props.color;

    let x0 = d3.scaleBand()
      .domain(data.map(d => d[groupKey]))
      .rangeRound([margin.left, width - margin.right])
      .paddingInner(0.1)

    let x1 = d3.scaleBand()
      .domain(keys)
      .rangeRound([0, x0.bandwidth()])
      .padding(0.05)

    let y = d3.scaleLinear()
      .domain([0, d3.max(data, d => d3.max(keys, key => d[key]))]).nice()
      .rangeRound([height - margin.bottom, margin.top])

    let xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x0).tickSizeOuter(0))
      .call(g => g.select(".domain").remove())

    let yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, "s"))
      .call(g => g.select(".domain").remove())
      .call(g => g.select(".tick:last-of-type text").clone()
        .attr("x", 3)
        .attr("text-anchor", "start")
        .attr("font-weight", "bold")
        .text(data.y))

    svg.append("g")
      .selectAll("g")
      .data(data)
      .join("g")
      .attr("transform", d => `translate(${x0(d[groupKey])},0)`)
      .selectAll("rect")
      .data(d => keys.map(key => ({ key, value: d[key] })))
      .join("rect")
      .attr("x", d => x1(d.key))
      .attr("y", d => y(d.value))
      .attr("width", x1.bandwidth())
      .attr("height", d => y(0) - y(d.value))
      .attr("fill", d => color(d.key));

    svg.append("g")
      .call(xAxis);

    svg.append("g")
      .call(yAxis);
  }

  render() {
    return this.createReturn();
  }
}
export default BarChart