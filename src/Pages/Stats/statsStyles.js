export const styles = theme => ({

    Page: {
      marginTop: "3.5em",
      background: theme.palette.fourth.main,
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      }
    },
    accountPage: {
      width: "100%",
      minHeight: "45em",
      margin: "auto",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    accountBox: {
      width: "80%",
      margin: "auto",
      marginBottom: "5%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      [theme.breakpoints.down('md')]: {
        width: "100%",
        flexDirection: "column",
      },
    },
    legendBox: {
      width: "85%",
      margin: "auto",
      marginTop: "1%",
      [theme.breakpoints.down('md')]: {
        width: "95%",
      },
    },
    legendGrid: {
      width: "100%",
      margin: "auto",
      background: theme.palette.secondary.main,
      border: '0',
      borderRadius: 2,
      display: "flex",
      flexDirection: "row",
      boxShadow: '0 3px 5px 2px rgba(227, 222, 211, .5)',
    },
    legendTitle: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "1em",
      marginRight: "2%",
      marginTop: "0.8em",
      marginBottom: "1em",
    },
    legendDatePicker: {
      fontFamily: theme.typo.fontFamily,
      margin: "1%",
      width: "7.5em",
    },
    leftBoxes: {
      width: "100%",
      margin: "0 1.5% 0 1.5%",
      [theme.breakpoints.down('md')]: {
        width: "95%",
        margin: "auto",
      },
    },
    logo: {
      display: "block",
      margin: "auto",
      paddingTop: "10%",
    },
    secondPage: {
      width: "100%",
      height: "55em",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
    },
    secondPageLeft: {
      marginLeft: "10%",
      marginTop: "7%",
      marginRight: "13%",
    },
    secondPageLeftButton: {
      fontFamily: theme.typo.fontFamily,
      background: theme.palette.primary.main,
      color: theme.palette.font1.color,
      margin: "1em 2em",
      padding: "0.5em",
    },
    secondPageLeftButtonBox: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
    },
    CardTitle: {
      fontFamily: theme.typo.fontFamily,
      paddingTop: "1%",
      paddingBottom: "3%",
    },
    CardText: {
      fontFamily: theme.typo.fontFamily,
      color: theme.palette.font2.color,
    },
    boxTitle: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "1em",
      marginTop: "0.8em",
      marginBottom: "1em",
    },
    box: {
      width: "100%",
      height: "auto",
      margin: "auto",
      marginTop: "6%",
      background: theme.palette.secondary.main,
      border: '0',
      borderRadius: 2,
      boxShadow: '0 3px 5px 2px rgba(227, 222, 211, .5)',
    },
    colorModal: {
      width: "100%",
      minHeight: "100%",
      padding: "auto",
      margin: "auto",
      background: theme.palette.primary.main,
    },
    colorModalTitle: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      paddingTop: "1em",
      paddingBottom: "1em",
      textAlign: "center",
      color: theme.palette.primary.main,
      background: theme.palette.secondary.main,
    },
    colorModalBox: {
      width: "35%",
      margin: "auto",
      marginTop: "5%",
      color: theme.palette.secondary.main,
      display: "flex",
      flexDirection: "column",
      [theme.breakpoints.down('md')]: {
        width: "50%",
      },
      [theme.breakpoints.down('sm')]: {
        width: "80%",
      }
    },
    colorModalButton: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "5%",
      marginRight: "5%",
      background: theme.palette.secondary.main,
      color: theme.palette.primary.main,
    },
    colorModalChromePicker:{
      display: "flex",
      flexDirection: "column",
      margin: "auto",
    },
    colorModalButtonBox: {
      width: "100%",
      display: "flex",
      flexDirection: "column",
      margin: "auto",
      marginTop: "3%",
    },
});