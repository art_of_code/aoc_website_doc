import React from "react";
import BarChart from './BarChart/BarChart';
import PieChart from './PieChart/PieChart';
import LineChart from './LineChart/LineChart';
import { ChromePicker } from 'react-color';
import { Auth } from 'aws-amplify';
import { API_URL } from '../../App.js';
import { Redirect } from 'react-router-dom';
import moment from "moment";
import * as d3 from "d3";
import DateFnsUtils from '@date-io/date-fns';
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers"
import { withStyles, Box, Typography, Grid, Modal, Divider, Button, Menu, MenuItem } from '@material-ui/core';

import { styles } from './statsStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

const options = [
  en.stat.temporalitySelect.day,
  en.stat.temporalitySelect.week,
  en.stat.temporalitySelect.month,
  en.stat.temporalitySelect.year
];

class Stats extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      colorIndex: 0,
      colorArray: ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac"],
      color: null,
      tmpColor: "",
      sorted: [],
      rawData: [],
      columns: [],
      lineSorted: [],
      barData: [],
      data: "",
      original: [],
      openInformations: false,
      selectedIndex: 0,
      anchorEl: null,
      menuState: false,
      startDay: moment().subtract(10, "days"),
      endDay: moment()
    }
    this.changeColor = this.changeColor.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
    this.menuClickItem = this.menuClickItem.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  selectedData(data, startDay, endDay) {
    let res = [];
    data.forEach((d, i) => {
      if (moment(d.date).isBetween(startDay, endDay)) {
        res.push(d);
      }
    });
    return res;
  }

  parseData = async (oui, startDay, endDay) => {
    let columns = [];
    let res = [];
    let data = this.selectedData(oui, startDay, endDay);
    data.forEach(d => {
      if (JSON.parse(d.foundErrors).statusCode === 200) {
        let tmpDate = new Date(d.date);

        let date;
        if (tmpDate.getMonth() + 1 >= 10) {
          date = tmpDate.getFullYear() + '-' + (tmpDate.getMonth() + 1) + '-' + (tmpDate.getDate() >= 10 ? tmpDate.getDate() : ("0" + tmpDate.getDate()));
        } else {
          date = tmpDate.getFullYear() + '-0' + (tmpDate.getMonth() + 1) + '-' + (tmpDate.getDate() >= 10 ? tmpDate.getDate() : ("0" + tmpDate.getDate()));
        }
        let obj = JSON.parse(JSON.parse(d.foundErrors).body);

        for (let key in obj) {
          if (key !== "internal" && key !== "predictable") {
            if (!columns.includes(key)) {
              columns.push(key);
            }
            obj[key].forEach(element => {
              res.push({ name: key, date: date });
            })
          }
        }
      }
    });
    let color = d3.scaleOrdinal()
      .range(this.state.colorArray)
    color.domain(columns);
    this.setState({ columns: columns, data: res, rawData: data, color: color, original: oui, startDay: startDay, endDay: endDay });
    this.legend();
  }

  legend() {
    let color = this.state.color;
    const node = this.node;
    let width = this.state.columns.length * 165;
    let height = this.state.columns.length * 40;
    const svg = d3.select(node)
      .append("svg")
      .attr("width", width)
      .attr("height", height)
    color.domain(this.state.columns);

    let legend = svg => {
      const g = svg
        .attr("transform", `translate(0,0)`)
        .attr("font-family", "sans-serif")
        .attr("font-size", 16)
        .selectAll("g")
        .data(color.domain().slice())
        .join("g")
        .attr("transform", (d, i) => `translate(${i * 165}, 0)`);

      g.append("rect")
        .attr("x", 0)
        .attr("width", 38)
        .attr("height", 38)
        .attr("fill", color);

      g.append("text")
        .attr("x", 45)
        .attr("y", 19)
        .attr("dy", "0.35em")
        .text(d => d);

      g.selectAll("rect")
        .on("click", (d, i) => {
          this.setState({ openInformations: true, colorIndex: this.state.colorArray.findIndex(f => f === this.state.color(d)) });
        })
    }
    svg.append("g")
      .call(legend);
  }

  changeColor(color) {
    let tmp = this.state.colorArray;
    tmp[this.state.colorIndex] = color.hex;
    let c = d3.scaleOrdinal()
      .range(tmp)
    c.domain(this.state.columns);
    this.setState({ colorArray: tmp, color: c, tmpColor: color.hex });
  }

  closeMenu() {
    this.setState({ anchorEl: null });
    this.setState({ menuState: false });
  }

  menuClickItem(event, index) {
    this.setState({ anchorEl: null, selectedIndex: index });
    this.setState({ menuState: false });
  }

  getData = async () => {
    let session = await Auth.currentSession();
    await fetch(API_URL + 'data', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${session.idToken.jwtToken}`,
      }
    }).then(res => res.json())
      .then((res) => {
        if (res.error) {
          console.log("ERROR: " + res.error);
        } else {
          this.parseData(res, this.state.startDay, this.state.endDay);
        }
      })
      .catch((e) => {
        console.log("ERROR: " + e)
      })
  }

  closeModal() {
    this.setState({ openInformations: false });
  }

  render() {
    const { classes } = this.props;

    if (this.state.redirect) {
      return (<Redirect to="login" />);
    }

    return (
      <div className={classes.Page}>
        <Box className={classes.accountPage}>
          <Box className={classes.legendBox}>
            <Grid
              className={classes.legendGrid}
              container
              direction="column"
              justify="flex-start"
            >
              <Typography variant="h5" className={classes.legendTitle}>
                {en.stat.legend}
              </Typography>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  disableFuture
                  className={classes.legendDatePicker}
                  openTo="date"
                  format="dd/MM/yyyy"
                  views={["year", "month", "date"]}
                  label={en.stat.startingDay}
                  value={this.state.startDay}
                  onChange={(date) => {
                    this.parseData(this.state.original, date, this.state.endDay);
                  }}
                />
                <DatePicker
                  disableFuture
                  className={classes.legendDatePicker}
                  openTo="date"
                  format="dd/MM/yyyy"
                  views={["year", "month", "date"]}
                  label={en.stat.endingDay}
                  value={this.state.endDay}
                  onChange={(date) => {
                    this.parseData(this.state.original, this.state.startDay, date);
                  }}
                />
              </MuiPickersUtilsProvider>
              <Menu
                id="lock-menu"
                anchorEl={this.state.anchorEl}
                keepMounted
                open={this.state.menuState}
                onClose={this.closeMenu}
              >
                {options.map((option, index) => (
                  <MenuItem
                    key={option}
                    disabled={index === -1}
                    selected={index === this.state.selectedIndex}
                    onClick={(event) => this.menuClickItem(event, index)}
                  >
                    {option}
                  </MenuItem>
                ))}
              </Menu>
              <svg style={{ paddingLeft: "1%", height: "4em", width: "100%", cursor: "pointer" }} ref={node => this.node = node}></svg>
              <Modal
                open={this.state.openInformations}
                onClose={() => { this.setState({ openInformations: false, error: "" }) }}
              >
                <Box className={classes.colorModal}>
                  <Typography className={classes.colorModalTitle} variant="h5">{en.stat.modal.title}</Typography>
                  <Divider variant="middle" />
                  <Box className={classes.colorModalBox}>
                    <ChromePicker onChangeComplete={this.changeColor} color={this.state.tmpColor} className={classes.colorModalChromePicker}></ChromePicker>
                    <Box className={classes.colorModalButtonBox}>
                      <Button className={classes.colorModalButton} onClick={() => { this.getData(); this.setState({ openInformations: false, error: "" }) }}>{en.stat.modal.close}</Button>
                    </Box>
                  </Box>
                </Box>
              </Modal>
            </Grid>
          </Box>
          <Box className={classes.accountBox}>
            <Box className={classes.leftBoxes}>
              <Grid
                className={classes.box}
                container
                direction="column"
                justify="flex-start"
              >
                <Typography variant="h5" className={classes.boxTitle}>
                  {en.stat.errornbr} {options[this.state.selectedIndex]}
                </Typography>
                <BarChart data={this.state.data}
                  width={600}
                  height={600}
                  columns={this.state.columns}
                  color={this.state.color}
                  temporality={options[this.state.selectedIndex]}
                ></BarChart>
              </Grid>
            </Box>
            <Box className={classes.leftBoxes}>
              <Grid
                className={classes.box}
                container
                direction="column"
                justify="flex-start"
              >
                <Typography variant="h5" className={classes.boxTitle}>
                  {en.stat.Testnbr} {options[this.state.selectedIndex]}
                </Typography>
                <LineChart data={this.state.rawData}
                  width={500}
                  height={400}
                  temporality={options[this.state.selectedIndex]} ></LineChart>
              </Grid>
            </Box>
            <Box className={classes.leftBoxes}>
              <Grid
                className={classes.box}
                container
                direction="column"
                justify="flex-start"
              >
                <Typography variant="h5" className={classes.boxTitle}>
                  {en.stat.errorRepartition}
                </Typography>
                <PieChart data={this.state.data} width={500} height={400} columns={this.state.columns} color={this.state.color}/>
              </Grid>
            </Box>
          </Box>
        </Box>
      </div>
    );
  }
}

export default withStyles(styles)(Stats);