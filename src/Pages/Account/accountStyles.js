export const styles = theme => ({
    Page: {
      marginTop: "3.5em",
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      }
    },
    accountPage: {
      width: "100%",
      minHeight: "45em",
      margin: "auto",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      background: theme.palette.fourth.main,
    },
    accountBox: {
      width: "80%",
      margin: "auto",
      marginBottom: 0,
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      [theme.breakpoints.down('md')]: {
        width: "100%",
        flexDirection: "column",
      },
    },
    errorMsg: {
      fontFamily: theme.typo.fontFamily,
      textAlign: "center",
      marginBottom: "2em",
    },
    loader: {
      margin: "auto",
      marginLeft: "1em",
      color: theme.palette.primary.main,
    },
    leftBoxes: {
      width: "60%",
      minHeight: "40em",
      margin: "auto",
      marginLeft: "5%",
      marginRight: "2%",
      [theme.breakpoints.down('md')]: {
        width: "95%",
        margin: "auto",
      },
    },
    rightBoxes: {
      width: "40%",
      minHeight: "40em",
      margin: "auto",
      marginLeft: "2%",
      marginRight: "5%",
      [theme.breakpoints.down('md')]: {
        width: "95%",
        margin: "auto",
        minHeight: "auto",
      },
    },
    box: {
      width: "100%",
      margin: "auto",
      marginTop: "6%",
      background: "#fff",
      border: '0',
      borderRadius: 5,
      boxShadow: '0 3px 5px 2px rgba(227, 222, 211, .5)',
    },
    boxTitle: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "0.4em",
      marginTop: "0.5em",
      marginBottom: "0.5em",
      marginRight: "30%",
      [theme.breakpoints.down('md')]: {
        marginRight: "auto",
      },
      [theme.breakpoints.down('sm')]: {
        fontSize: "1.2em",
      },
    },
    boxSave: {
      color: theme.palette.third.main,
    },
    boxInformationsSeparator: {
      display: "flex",
      flexDirection: "row",
      [theme.breakpoints.down('md')]: {
        marginBottom: "1em",
      },
    },
    boxInformationsLeft: {
      width: "35%",
      margin: "auto 1em auto 1em",
      justifyContent: "center",
      alignItems: "center",
    },
    boxInformationsAvatar: {
      background: "#fff",
      color: theme.palette.font1.color,
      height: "5em",
      width: "5em",
      margin: "auto",
      marginBottom: "0.5em",
      cursor: "pointer",
    },
    boxInformationsAvatarEmpty: {
      background: theme.palette.primary.main,
      color: theme.palette.font1.color,
      height: "5em",
      width: "5em",
      margin: "auto",
      marginBottom: "0.5em",
      cursor: "pointer",
    },
    boxInformationsRight: {
      width: "65%",
      margin: "auto 1em auto 1em",
    },
    boxInformationsUsername: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "1em",
      marginBottom: "0.2em",
      [theme.breakpoints.down('sm')]: {
        fontSize: "1.2em",
      },
    },
    boxInformationsRealname: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "1.5em",
      marginBottom: "1em",
      [theme.breakpoints.down('sm')]: {
        fontSize: "1.2em",
      },
    },
    boxInformationsButtonBox: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      margin: "auto",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
    },
    boxInformationsButton: {
      fontFamily: theme.typo.fontFamily,
      width: "70%",
      padding: "auto",
      margin: "2em auto 1em auto",
      background: theme.palette.primary.main,
      color: theme.palette.font1.color,
      [theme.breakpoints.down('md')]: {
        width: "100%",
        margin: "auto",
        fontSize: "0.6em",
      },
    },
    phoneContainer: {
      width: "80% !important",
      margin: "auto",
      [theme.breakpoints.down('sm')]: {
        width: "95% !important",
      },
    },
    phoneInput: {
      fontFamily: theme.typo.fontFamily,
      width: "100% !important",
      background: theme.palette.secondary.main,
    },
    boxOptionsUpgrade: {
      fontFamily: theme.typo.fontFamily,
      color: theme.palette.third.main,
    },
    boxOptionsLogout: {
      fontFamily: theme.typo.fontFamily,
      color: "red",
    },
    boxEmail: {
      marginLeft: "2em",
      marginBottom: "1em",
      [theme.breakpoints.down('xs')]: {
        marginLeft: "0.5em",
      },
    },
    boxPhoneNumber: {
      marginLeft: "2em",
      marginBottom: "1em",
      [theme.breakpoints.down('xs')]: {
        marginLeft: "0.5em",
      },
    },
    labelfile: {
      fontFamily: theme.typo.fontFamily,
      cursor: "pointer",
      margin: "auto",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      fontSize: "1em",
      [theme.breakpoints.down('sm')]: {
        fontSize: "0.8em",
      },
      color: theme.palette.third.main,
    },
    inputfile: {
      display: "none",
    },
    modalError: {
      fontFamily: theme.typo.fontFamily,
      marginTop: "2.5%",
      marginBottom: "2.5%",
      textAlign: "center"
    },
    modal: {
      width: "100%",
      minHeight: "100%",
      padding: "auto",
      margin: "auto",
      background: theme.palette.primary.main,
    },
    modalPhone: {
      width: "100%",
      minHeight: "100%",
      background: theme.palette.primary.main,
      paddingBottom: "20%",
      [theme.breakpoints.down('sm')]: {
        paddingBottom: "50%",
      },
    },
    modalTitle: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      paddingTop: "1em",
      paddingBottom: "1em",
      textAlign: "center",
      color: theme.palette.primary.main,
      background: theme.palette.secondary.main,
    },
    modalSubject: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      paddingTop: "1em",
      paddingBottom: "1em",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      color: theme.palette.secondary.main,
      fontSize: "2em",
      [theme.breakpoints.down('sm')]: {
        fontSize: "1em",
      }
    },
    modalBox: {
      width: "90%",
      margin: "auto",
      marginTop: "5%",
      color: theme.palette.secondary.main,
      flexDirection: "column",
    },
    modalField: {
      fontFamily: theme.typo.fontFamily,
      width: "100%",
      marginTop: "1em",
      marginBottom: "1em",
      background: theme.palette.secondary.main,
    },
    modalButtonBox: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      marginTop: "3%",
      marginBottom: "4%",
    },
    modalButton: {
      fontFamily: theme.typo.fontFamily,
      marginLeft: "5%",
      marginRight: "5%",
      background: theme.palette.secondary.main,
      color: theme.palette.primary.main,
      width: "30%",
      [theme.breakpoints.down('sm')]: {
        width: "50%",
      }
    },
  });