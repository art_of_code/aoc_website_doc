import React from "react";
import { Redirect } from "react-router-dom";
import { Divider, Avatar, CircularProgress, withStyles, Typography, Button, Box, Grid, TextField } from '@material-ui/core';
import { Dialog, List, ListItem, ListItemText, ListItemSecondaryAction } from '@material-ui/core';
import { Auth, Storage } from 'aws-amplify';
import { API_URL } from '../../App.js';
import { connect } from "react-redux"
import { _onCheckLog } from "../../Redux/Store/actionCreators"

import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/material.css'

import { styles } from './accountStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

const mapStateToProps = state => {
  return {
    account: state.userInfo,
    imgLink: state.imgLink
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveAccount: () => { dispatch(_onCheckLog()) },
  }
}

class Account extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newEmail: "",
      newPhoneNumber: "",
      password: "",
      confirmPassword: "",
      oldPassword: "",
      error: null,
      loader: false,
      openInformations: false,
      openEmail: false,
      openDelete: false,
      openPhone: false,
      code: "",
      step: 1,
    };
  }

  _onLogout = async (event) => {
    Auth.signOut()
      .then(() => {
        this.setState({ redirect: true });
      })
      .catch(err => console.log(err));
  }

  _onChangeInformation = async () => {
    this.setState({ error: "", loader: true });
    if (this.state.password === this.state.confirmPassword) {
      Auth.currentAuthenticatedUser()
        .then(user => {
          return Auth.changePassword(user, this.state.oldPassword, this.state.password);
        })
        .then(() => {
          this.setState({ openInformations: false, oldPassword: "" });
        })
        .catch(err => {
          this.setState({ error: err.message });
        });
    } else {
      this.setState({ error: "Password not matching" });
    }
    this.setState({ loader: false });
  };

  // image link
  async onChange(e) {
    this.setState({ error: "", loader: true });
    const file = e.target.files[0];
    Storage.put((await Auth.currentUserInfo()).username, file)
      .then(async () => {
        Storage.get((await Auth.currentUserInfo()).username)
          .then(result => {
            window.location.reload(false);
          })
          .catch(() => {
            this.setState({ error: "Internal error" })
            this.setState({ loader: false });
          });
      })
      .catch(() => {
        this.setState({ error: "Internal error" })
        this.setState({ loader: false });
      });

  }

  _onSaveEmail = async () => {
    this.setState({ loader: true, error: "" });
    /* Save the change here */
    let user = await Auth.currentAuthenticatedUser();
    Auth.updateUserAttributes(user, {
      email: this.state.newEmail
    })
      .then((result) => {
        if (result !== "SUCCESS") {
          this.setState({ error: "Error while updating your informations" });
        } else {
          Auth.verifyCurrentUserAttribute("email")
            .then(() => {
              this.setState({ error: "a verification code have been sent to your new Email address", step: 2 });
            })
            .catch((e) => {
              this.setState({ error: e.message });
            });
        }
      })
      .catch(e => {
        this.setState({ error: e.message });
      });
  };

  _onVerifyEmailCode = async () => {
    this.setState({ loader: true, error: "" });
    Auth.verifyCurrentUserAttributeSubmit("email", this.state.code)
      .then(() => {
        this.setState({ loader: false, step: 1, openEmail: false, code: "", newEmail: "" });
        this.props.saveAccount();
      })
      .catch(e => {
        this.setState({ error: e.message });
      });
  };

  _onDeleteAccount = async () => {
    this.setState({ loader: true, error: "" });
    let session = await Auth.currentSession();
    await fetch(API_URL + 'backoffice/deleteMe', {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${session.idToken.jwtToken}`,
        'Content-Type': "application/json",
      },
      body: JSON.stringify({ 'accessToken': session.accessToken.jwtToken })
    })
      .then(() => {
        //SUCCESS
        Auth.signOut()
          .then(() => {
            this.setState({ redirect: true });
          })
          .catch(err => console.log(err));
      })
      .catch((e) => {
        // FAILURE
        console.log(e);
        this.setState({ error: "Internal Error" });
      });
    this.setState({ loader: false });
  }

  _onSaveNumber = async () => {
    this.setState({ loader: true, error: "" });
    /* Save the change here */
    let user = await Auth.currentAuthenticatedUser();
    await Auth.updateUserAttributes(user, {
      phone_number: '+' + this.state.newPhoneNumber
    })
      .then((result) => {
        if (result !== "SUCCESS") {
          this.setState({ error: "Error while updating your informations" });
        } else {
          this.setState({ loader: false, openPhone: false, newPhoneNumber: "" });
          this.props.saveAccount();
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({ error: e.message });
      });
  };

  render() {
    const { classes } = this.props;

    if (this.state.redirect) {
      return (<Redirect to="login" />);
    }
    return (
      <div className={classes.Page}>
        <div className={classes.contentPage}>

          {/* Add code page here */}
          <Box className={classes.accountPage}>
            <Box className={classes.accountBox}>
              <Box className={classes.leftBoxes}>
                {/* Box Informations */}
                <Grid
                  className={classes.box}
                  container
                  direction="column"
                  justify="flex-start"
                >
                  <List component="nav" aria-label="Options">
                    <ListItem>
                      <Typography variant="h5" className={classes.boxTitle}>{en.account.informationTitle}
                        {(() => {
                          switch (this.state.loader) {
                            case true: return <CircularProgress className={classes.loader} />;
                            default: return null;
                          }
                        })()}
                      </Typography>
                    </ListItem>
                  </List>
                  <Box className={classes.boxInformationsSeparator}>
                    <Box className={classes.boxInformationsLeft}>
                      {(() => {
                        if (this.props.imgLink.imgLink === null) {
                          return <label htmlFor="file"><Avatar className={classes.boxInformationsAvatarEmpty} /></label>
                        } else {
                          return <label htmlFor="file"><Avatar src={this.props.imgLink.imgLink} className={classes.boxInformationsAvatar} /></label>
                        }
                      })()}
                      <label htmlFor="file" className={classes.labelfile}>{en.account.informationUpdatePhoto}</label>
                      <input
                        className={classes.inputfile}
                        id="file"
                        type="file" accept='image/*'
                        onChange={(evt) => this.onChange(evt)}
                      />
                    </Box>
                    <Box className={classes.boxInformationsRight}>
                      <Typography variant="h5" className={classes.boxInformationsUsername}>{en.account.informationUsername}: {this.props.account.username}</Typography>
                      <Typography variant="h6" className={classes.boxInformationsRealname}>{en.account.informationRealName}:  {this.props.account.realname}</Typography>
                      <Divider />
                      <Box className={classes.boxInformationsButtonBox}>
                        <Button className={classes.boxInformationsButton} onClick={() => { this.setState({ openInformations: true }); }} variant="contained">{en.account.informationEditPassword}</Button>
                        <Dialog
                          open={this.state.openInformations}
                          onClose={() => { this.setState({ openInformations: false }) }}
                          keepMounted
                          aria-labelledby={en.login.modal.title}
                          aria-describedby={en.login.modal.title}
                          fullWidth={true}
                          maxWidth="md"
                        >
                          <Box className={classes.modal}>
                            <Typography className={classes.modalTitle} variant="h5">{en.account.modalInformations.title}</Typography>
                            <Divider variant="middle" />
                            <Box className={classes.modalBox}>
                              <span className={classes.modalError}>{this.state.error}</span>
                              <TextField className={classes.modalField} type="password" label={en.account.modalInformations.newPassword} value={this.state.password} variant="outlined" onChange={e => this.setState({ password: e.target.value, passwordDisplay: "visible" })} />
                              <TextField className={classes.modalField} type="password" label={en.account.modalInformations.confirmPassword} value={this.state.confirmPassword} variant="outlined" onChange={e => this.setState({ confirmPassword: e.target.value })} />
                              <TextField className={classes.modalField} type="password" label={en.account.modalInformations.oldPassword} value={this.state.oldPassword} variant="outlined" onChange={e => this.setState({ oldPassword: e.target.value })} />
                              <Box className={classes.modalButtonBox}>
                                <Button className={classes.modalButton} onClick={() => { this.setState({ loader: false, openInformations: false, password: "", confirmPassword: "", oldPassword: "", error: "" }) }} variant="contained">{en.account.modalInformations.cancel}</Button>
                                <Button className={classes.modalButton} onClick={() => { this._onChangeInformation(); }} variant="contained">{en.account.modalInformations.confirm}</Button>
                              </Box>
                            </Box>
                          </Box>
                        </Dialog>
                      </Box>
                    </Box>
                  </Box>
                </Grid>

                {/* Box Options */}
                <Grid
                  className={classes.box}
                  container
                  direction="column"
                  justify="flex-start"
                >
                  <List component="nav" aria-label="Options">
                    <ListItem >
                      <ListItemText><Typography className={classes.boxTitle} variant="h5">{en.account.optionTitle}
                        {(() => {
                          switch (this.state.loader) {
                            case true: return <CircularProgress className={classes.loader} />;
                            default: return null;
                          }
                        })()}
                      </Typography></ListItemText>
                    </ListItem>
                  </List>

                  <List component="nav" aria-label="Options">
                    <ListItem button onClick={() => { window.open('https://artofcode.eu/', '_blank') }}>
                      <ListItemText className={classes.boxOptionsUpgrade}>{en.account.optionUpgrade}</ListItemText>
                    </ListItem>
                    <Divider />
                    <ListItem button onClick={() => { this.setState({ openDelete: true }); }}>
                      <ListItemText className={classes.boxOptionsLogout}>{en.account.optionDelete}</ListItemText>
                    </ListItem>
                    <Dialog
                      open={this.state.openDelete}
                      onClose={() => { this.setState({ openDelete: false }) }}
                      keepMounted
                      aria-labelledby={en.login.modal.title}
                      aria-describedby={en.login.modal.title}
                      fullWidth={true}
                      maxWidth="md"
                    >
                      <Box className={classes.modal}>
                        <Typography className={classes.modalTitle} variant="h5">{en.account.modalOptions.title}</Typography>
                        <Divider variant="middle" />
                        <Box className={classes.modalBox}>
                          <span className={classes.modalError}>{this.state.error}</span>
                          <Typography className={classes.modalSubject}>{en.account.modalOptions.subject}</Typography>
                          <Box className={classes.modalButtonBox}>
                            <Button className={classes.modalButton} onClick={() => { this.setState({ loader: false, openDelete: false, error: "" }) }} variant="contained">{en.account.modalOptions.cancel}</Button>
                            <Button id="gtm-delete" className={classes.modalButton} onClick={() => { this._onDeleteAccount(); }} variant="contained">{en.account.modalOptions.confirm}</Button>
                          </Box>
                        </Box>
                      </Box>
                    </Dialog>
                    <Divider light />
                    <ListItem button onClick={this._onLogout}>
                      <ListItemText className={classes.boxOptionsLogout}>{en.account.optionLogout}</ListItemText>
                    </ListItem>
                  </List>
                </Grid>
              </Box>

              <Box className={classes.rightBoxes}>
                {/* Box Adress */}
                <Grid
                  className={classes.box}
                  container
                  direction="column"
                  justify="flex-start"
                >
                  <List component="nav" aria-label="Email">
                    <ListItem >
                      <ListItemText><Typography className={classes.boxTitle} variant="h5">{en.account.emailTitle}
                        {(() => {
                          switch (this.state.loader) {
                            case true: return <CircularProgress className={classes.loader} />;
                            default: return null;
                          }
                        })()}
                      </Typography></ListItemText>
                      <ListItemSecondaryAction><ListItemText>
                        <Button className={classes.boxSave} onClick={() => { this.setState({ openEmail: true }); }}>{en.account.edit}</Button>
                        <Dialog
                          open={this.state.openEmail}
                          onClose={() => { this.setState({ openEmail: false }) }}
                          keepMounted
                          aria-labelledby={en.login.modal.title}
                          aria-describedby={en.login.modal.title}
                          fullWidth={true}
                          maxWidth="md"
                        >
                          <Box className={classes.modal}>
                            <Typography className={classes.modalTitle} variant="h5">{en.account.modalEmail.title}</Typography>
                            <Divider variant="middle" />
                            {(() => {
                              switch (this.state.step) {
                                case 1:
                                  return <Box className={classes.modalBox}>
                                    <span className={classes.modalError}>{this.state.error}</span>
                                    <TextField className={classes.modalField} type="email" label={en.account.modalEmail.newEmail} value={this.state.newEmail} variant="outlined" onChange={e => this.setState({ newEmail: e.target.value })} />
                                    <Box className={classes.modalButtonBox}>
                                      <Button className={classes.modalButton} onClick={() => { this.setState({ loader: false, openEmail: false, newEmail: "", error: "" }) }} variant="contained">{en.account.modalEmail.cancel}</Button>
                                      <Button className={classes.modalButton} onClick={() => { this._onSaveEmail(); }} variant="contained">{en.account.modalEmail.confirm}</Button>
                                    </Box>
                                  </Box>
                                case 2:
                                  return <Box className={classes.modalBox}>
                                    <Typography className={classes.modalTitle} variant="h5">{en.account.modalEmail.subject}</Typography>
                                    <span className={classes.modalError}>{this.state.error}</span>
                                    <TextField className={classes.modalField} type="text" label={en.account.modalEmail.code} value={this.state.code} variant="outlined" onChange={e => this.setState({ code: e.target.value })} />
                                    <Box className={classes.modalButtonBox}>
                                      <Button className={classes.modalButton} onClick={() => { this.setState({ newEmail: "", step: 1 }) }} variant="contained">{en.account.modalEmail.back}</Button>
                                      <Button className={classes.modalButton} onClick={() => { this._onVerifyEmailCode(); }} variant="contained">{en.account.modalEmail.validate}</Button>
                                    </Box>
                                  </Box>
                                default:
                                  return <Box className={classes.modalBox}>
                                    <Typography className={classes.modalTitle} variant="h5">{en.account.modalEmail.subject}</Typography>
                                    <span className={classes.modalError}>{this.state.error}</span>
                                    <TextField className={classes.modalField} type="text" label={en.account.modalEmail.code} value={this.state.code} variant="outlined" onChange={e => this.setState({ code: e.target.value })} />
                                    <Box className={classes.modalButtonBox}>
                                      <Button className={classes.modalButton} onClick={() => { this.setState({ newEmail: "", step: 1 }) }} variant="contained">{en.account.modalEmail.back}</Button>
                                      <Button className={classes.modalButton} onClick={() => { this._onVerifyEmailCode(); }} variant="contained">{en.account.modalEmail.validate}</Button>
                                    </Box>
                                  </Box>
                              }
                            })()}
                          </Box>
                        </Dialog>

                      </ListItemText></ListItemSecondaryAction>
                    </ListItem>
                  </List>
                  <Typography variant="h6" className={classes.boxEmail}>{this.props.account.email}</Typography>
                </Grid>

                {/* Box Phone Number */}
                <Grid
                  className={classes.box}
                  container
                  direction="column"
                  justify="flex-start"
                >
                  <List component="nav" aria-label="Phone">
                    <ListItem >
                      <ListItemText><Typography className={classes.boxTitle} variant="h5">{en.account.phoneTitle}
                        {(() => {
                          switch (this.state.loader) {
                            case true: return <CircularProgress className={classes.loader} />;
                            default: return null;
                          }
                        })()}
                      </Typography></ListItemText>
                      <ListItemSecondaryAction><ListItemText>
                        <Button className={classes.boxSave} onClick={() => { this.setState({ openPhone: true }); }}>{en.account.edit}</Button>
                        <Dialog
                          open={this.state.openPhone}
                          onClose={() => { this.setState({ openPhone: false }) }}
                          keepMounted
                          aria-labelledby={en.login.modal.title}
                          aria-describedby={en.login.modal.title}
                          fullWidth={true}
                          maxWidth="md"
                        >
                          <Box className={classes.modalPhone}>
                            <Typography className={classes.modalTitle} variant="h5">{en.account.modalPhone.title}</Typography>
                            <Divider variant="middle" />
                            <Box className={classes.modalBox}>
                              <span className={classes.modalError}>{this.state.error}</span>
                              <PhoneInput
                                enableSearch='true'
                                specialLabel=''
                                containerClass={classes.phoneContainer}
                                inputClass={classes.phoneInput}
                                country={'fr'}
                                placeholder={en.account.modalPhone.field}
                                isValid={(inputNumber, country) => {
                                  if (inputNumber.length === country.format.replace(/\s/g, '').replace('+', '').length) {
                                    return true;
                                  } else {
                                    return false;
                                  }
                                }}
                                value={this.state.newPhoneNumber}
                                onChange={(newPhoneNumber) => { this.setState({ newPhoneNumber }) }}
                              />
                              <Box className={classes.modalButtonBox}>
                                <Button className={classes.modalButton} onClick={() => { this.setState({ loader: false, openPhone: false, oldPassword: "", error: "" }) }} variant="contained">{en.account.modalPhone.cancel}</Button>
                                <Button className={classes.modalButton} onClick={() => { this._onSaveNumber(); }} variant="contained">{en.account.modalPhone.confirm}</Button>
                              </Box>
                            </Box>
                          </Box>
                        </Dialog>

                      </ListItemText></ListItemSecondaryAction>
                    </ListItem>
                  </List>
                  {(() => {
                    switch (this.props.account.phoneNumber) {
                      case undefined: return <Typography variant="h6" className={classes.boxPhoneNumber}>{en.account.phoneNotMentioned}</Typography>;
                      default: return <Typography variant="h6" className={classes.boxPhoneNumber}>{this.props.account.phoneNumber}</Typography>;
                    }
                  })()}
                </Grid>
              </Box>
            </Box>
            <Typography variant="h6" className={classes.errorMsg}>{this.state.error}</Typography>
          </Box>
          {/* End code page here */}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Account));