export const styles = theme => ({
    Page: {
      width: "100%",
      marginTop: "3.5em",
      background: theme.palette.fourth.main,
      [theme.breakpoints.up('sm')]: {
        marginTop: "4em",
      },
      paddingBottom: "10%"
    },
    Content: {
      width: "100%",
      margin: "auto",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    BoxContent: {
      width: "100%",
      margin: "auto",
      marginBottom: "10%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
    },
    Card: {
      maxWidth: "100%",
      marginTop: "13%",
      background: theme.palette.secondary.main,
      [theme.breakpoints.down('md')]: {
        marginTop: "10%",
      }
    },
    vsCodeLogo: {
      height: 0,
      paddingTop: '56.25%',
      backgroundSize: "contain",
    },
    vsCodeText: {
      fontFamily: theme.typo.fontFamily,
      display: "flex",
      alignItem: "center",
      justifyContent: "center",
      textAlign: 'center',
    },
    button: {
      fontFamily: theme.typo.fontFamily,
      display: "flex",
      alignItem: "center",
      justifyContent: "center",
      textAlign: 'center',
    }
});