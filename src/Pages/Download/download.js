import React from "react";
import { Box, Button, Card, CardMedia, CardContent, Typography, CardActionArea, CardActions } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

import { styles } from './downloadStyles';

const lang = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
const en = (lang.substring(0, 2) === 'fr') ? require("../../Language/fr.js").default : require("../../Language/en.js").default;

class Download extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      redirect: false,
      menuAppear: false,
    };

    this._toggleMenu = this._toggleMenu.bind(this);
    this._download = this._download.bind(this);
  }

  _toggleMenu = () => {
    this.setState({ menuAppear: !this.state.menuAppear });
  }

  _download = () => {
    fetch('https://aoc-backoffice.s3.eu-west-2.amazonaws.com/artofcode-beta-0.0.4.vsix')
      .then(r => {
        console.log(r);
        r.blob().then(blob => {
          console.log(blob);
          const url = window.URL.createObjectURL(blob);
          const a = document.createElement('a');
          a.style.display = 'none';
          a.href = url;
          a.download = 'ArtofCode.vsix';
          document.body.appendChild(a);
          a.click();
          window.URL.revokeObjectURL(url);
        })
      })
      .catch(e => console.log(e));
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.Page}>
        <Box className={classes.Content}>
          <Box className={classes.BoxContent}>
            <Card className={classes.Card}>
              <CardActionArea onClick={this._download}>
                <CardMedia
                  className={classes.vsCodeLogo}
                  image="img/vscode.png"
                  title="Visual Studio Code Logo"></CardMedia>
                <CardContent className={classes.vsCodeText}>
                  <Typography>
                    {en.download.download}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button className={classes.button} size="small" color="primary" component="a" href="https://code.visualstudio.com/docs/editor/extension-gallery#_install-from-a-vsix" target="_blank">
                  {en.download.install}
                </Button>
                <Button className={classes.button} size="small" color="primary" component="a" href="https://backoffice.artofcode.eu/doc-page/5f00a0ba11aaa70008a2b3e2">
                  {en.download.documentation}
                </Button>
              </CardActions>
            </Card>
          </Box>
        </Box>
      </div>
    );
  }
}

export default withStyles(styles)(Download);
