import React, { useState, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Login from "./Pages/Connection/Login/login";
import Register from "./Pages/Connection/Register/register";
import Recover from "./Pages/Connection/Recover/recover";

import DocPage from "./Pages/Documentation/DocPage/DocPage";
import CreateDocPage from "./Pages/Documentation/CreateDocPage/CreateDocPage";
import DocList from "./Pages/Documentation/DocList/DocList";

import Stats from "./Pages/Stats/stats";
import Home from "./Pages/Home/home";
import Account from "./Pages/Account/account";
import Analyser from "./Pages/Analyser/analyser";
import Download from "./Pages/Download/download";
//import { useTracking } from './useTracking';

import NotFound from "./Pages/NotFound/notFound";
import { Auth } from "aws-amplify";

function PrivateRoute({ children, ...rest }) {
  const [user, setUser] = useState(false);
  const [waiting, setWaiting] = useState(true);

  useEffect(() => {
    Auth.currentSession()
      .then(() => {
        setUser(true);
        setWaiting(false);
      })
      .catch(() => {
        setUser(false);
        setWaiting(false);
      })
  })

  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (waiting) return null;
        else {
          if (user) {
            return children;
          } else {
            return <Redirect to={{
              pathname: '/Login',
              state: { from: location }
            }} />;
          }
        }
      }} />
  );
}

const Routes = () => {
  // useTracking();

  return (
    <Switch >
      <Route exact path="/" component={Home} />
      <Route exact path="/Login" component={Login} />
      <Route exact path="/Register" component={Register} />
      <Route exact path="/Recover" component={Recover} />
      <Route exact path="/DocumentationPage/:pageId" component={DocPage} />
      <Route exact path="/DocumentationList" component={DocList} />
      <PrivateRoute exact path="/Create-doc">
        <CreateDocPage />
      </PrivateRoute>
      <PrivateRoute exact path="/Account">
        <Account />
      </PrivateRoute>
      <PrivateRoute exact path="/Analyser">
        <Analyser />
      </PrivateRoute>
      <PrivateRoute exact path="/Stats">
        <Stats />
      </PrivateRoute>
      <PrivateRoute exact path="/Download">
        <Download />
      </PrivateRoute>

      { /* Not found Page */}
      <Route component={NotFound} status={404} />
    </Switch>
  );
}

export default Routes;
