import * as actionTypes from "./actionTypes"
import { Auth, Storage } from 'aws-amplify';

export const checkLog = (userInfo, imgLink) => {
  return {
    type: actionTypes.CHECK_LOG,
    userInfo,
    imgLink,
  }
}

export const _onCheckLog = () => {
  return dispatch => {
    Auth.currentSession()
    .then(() => {
    // USER IS LOGGED
      Auth.currentUserInfo()
      .then(user => {
        // USER INFORMATIONS
        Storage.get(user.username)
          .then(imgLink => {
            dispatch(checkLog(user.attributes, imgLink))
          })
          .catch((e) => {
            console.log(e)
            dispatch(checkLog(user.attributes, null))
          });
      })
      .catch(e => console.log(e));
    })
    .catch((e) => console.log(e));
  }
}