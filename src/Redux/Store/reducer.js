import * as actionTypes from "./actionTypes"

const initialState = {
  userInfo: {},
  imgLink: {},
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHECK_LOG:
      const newUserInfo = {
        username: action.userInfo.nickname,
        email: action.userInfo.email,
        realname: action.userInfo.name,
        phoneNumber: action.userInfo.phone_number,
      }
      const newImgLink = {
        imgLink: action.imgLink,
      }
      return {
        userInfo: newUserInfo,
        imgLink: newImgLink
      }
    default:
      break;
  }
  return state
}
export default reducer