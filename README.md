## React

This project work with ReactJs, and the project using MaterialUi as bootstrap to design the Website.
To know more about MaterialUi, we recommend you to go on the [Official Documentation website](https://material-ui.com/)

This project is based on a One Page format.

## npm

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## The Project

### Component

The project contain a multiple number of component which allow you to copy paste them to respect the Graphical Chart and offer you a lot of time saving.
to see all the component already available, please refer you to the Graphical Chart of the project. 

### API

This project use an API with differents routes to get, send or change informations.
The actual Routes are :

| Routes   | Method |      description      |
|----------|:------:|----------------------:|
| /backoffice/login | POST | Receive a Json who contain an email and a password. If the connection succeed, the API return an OAUTH token. |
| /backoffice/register | POST | Receive a Json who contain an email, password and a username. Send an email verification at the registered email. |
| /backoffice/check/JWToken | GET | Receive the id/token of the user. If the user is validate, return a value verified to True. |
| /backoffice/process | POST | Receive a file who's sent to the parser. It return the tree of the source file and the list of the erreur found |


### Fetch

To communicate with the API, we're using Fetch in Asynchrone Mode. Fetch need to get as parameters the body if there is some parameters to send to the API. The body need to be stringify.
A header need to be set, to tell to the API which type of content we need.
And of course, a .then to get the return of the API and a .catch to get the error return if they are. 

### AWS Amplify

The Amplify Framework uses Amazon Cognito as the main authentication provider.
Amazon Cognito is a robust user directory service that handles user registration, authentication, account recovery & other operations.
with this link, you’ll learn how to add authentication to your application using Amazon Cognito and username/password login.
[Aws Amplify Cognito](https://docs.amplify.aws/lib/auth/getting-started/q/platform/js)


### Popup

To start using reactjs popup you just need to import the component from the reactjs-popup package.

```
import React from "react";
import Popup from "reactjs-popup";
 
export default () => (
  <Popup trigger={<button> Trigger</button>} position="right center">
    <div>Popup content here !!</div>
  </Popup>
);
```

To view some exmaple : https://react-popup.elazizi.com/